﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HudController : Kameosa.GameObject.Common.Controller.BaseHudController
{
    [SerializeField]
    private GameplayController gameplayController;

	protected override void Awake()
	{
		base.Awake();

        if (this.gameplayController == null)
        {
            this.gameplayController = GameObject.Find("Gameplay").GetComponent<GameplayController>();
        }
	}

	protected override void Start()
	{
		base.Start();
	}

    private void Update()
    {
    }

	protected override void OnGameAwake()
	{
		base.OnGameAwake();
	}

	public override void GameStart()
	{
		base.GameStart();
	}

	public override void GamePause()
	{
		base.GamePause();
	}

	public override void GameResume()
	{
		base.GameResume();
	}

    protected override void OnGameWon()
    {
        base.OnGameWon();
    }

    protected override void OnGameLose()
    {
        base.OnGameLose();
    }

    protected override void OnGameEnd()
    {
        base.OnGameEnd();
    }

	public override void GameRestart()
	{
		base.GameRestart();
	}

	public override void GameQuit()
	{
		base.GameQuit();
	}
}