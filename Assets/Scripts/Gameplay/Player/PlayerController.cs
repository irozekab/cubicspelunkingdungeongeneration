﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Kameosa.Gameplay2D;

public class PlayerController : Kameosa.GameObject.Gameplay2D.Common.Controller.BasePlayerController
{
    [SerializeField]
    private GameplayController gameplayController;

    protected override void Awake()
    {
        base.Awake();

        if (this.gameplayController == null)
        {
            this.gameplayController = GameObject.Find("Gameplay").GetComponent<GameplayController>();
        }
    }

    protected override void Start()
    {
        base.Start();
    }
    
    private void Update() 
    {
    }

    protected override void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);
    }

    protected override void Die()
    {
        base.Die();

        //this.animator.Play("Die");
        //EndRound();
    }

	protected override void OnGameAwake()
	{
		base.OnGameAwake();
	}

	protected override void OnGameStart()
	{
		base.OnGameStart();
	}

	protected override void OnGamePause()
	{
		base.OnGamePause();
	}

	protected override void OnGameResume()
	{
		base.OnGameResume();
	}

    protected override void OnGameWon()
    {
        base.OnGameWon();
    }

    protected override void OnGameLose()
    {
        base.OnGameLose();
    }

	protected override void OnGameEnd()
	{
		base.OnGameEnd();
	}

	protected override void OnGameRestart()
	{
		base.OnGameRestart();
	}

	protected override void OnGameQuit()
	{
		base.OnGameQuit();
	}
}