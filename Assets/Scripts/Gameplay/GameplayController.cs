﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Kameosa.GameObject.Common.Controller;
using Kameosa.GameObject.Gameplay2D.Platformer.Generation.Spelunky;

public class GameplayController : Kameosa.GameObject.Common.Controller.BaseGameplayController 
{
    [SerializeField]
    private PlayerController playerController;

    [SerializeField]
    private DungeonController dungeonController;

    //private int score = 0;

    #region Properties

    //public int Score
    //{
    //    get
    //    {
    //        return this.score;
    //    }
    //}

    #endregion

    private void Awake()
    {
        if (this.playerController == null)
        {
            this.playerController = GameObject.Find("Player").GetComponent<PlayerController>();
        }

        if (this.dungeonController == null)
        {
            this.dungeonController = UnityEngine.GameObject.Find("Dungeon").GetComponent<DungeonController>();
        }
    }

    private void OnEnable()
    {
        //this.playerController.OnDie += OnPlayerDie;
        this.dungeonController.OnDungeonGenerated += OnDungeonGenerated;
    }

    private void Start()
    {
    }

    private void Update()
    {
    }

    private void OnDisable()
    {
        //this.playerController.OnDie -= OnPlayerDie;
    }

    protected override void GameAwake()
    {
		base.GameAwake();
    }
    
    public override void GameStart()
    {
		base.GameStart();

        Time.timeScale = 1f;
    }

    public override void GamePause()
    {
		base.GamePause();

        Time.timeScale = 0f;
    }

    public override void GameResume()
    {
		base.GameResume();

        Time.timeScale = 1f;
    } 

    protected override void GameWon()
    {
		base.GameWon();
    } 

    protected override void GameLose()
    {
		base.GameLose();
    } 

    protected override void GameEnd()
    {
		base.GameEnd();
 
        Time.timeScale = 0f;
    } 

    public override void GameRestart()
    {
		base.GameRestart();

        SceneTransitionController.Instance.ReloadScene();
        StartCoroutine(UnfreezeTimeScale());
    }

    public override void GameQuit()
    {
		base.GameQuit();

        SceneTransitionController.Instance.LoadScene("MainMenu");
        StartCoroutine(UnfreezeTimeScale());
    }

    //private void OnPlayerDie()
    //{
    //    GameLose();
    //}

    //private void OnGameWon()
    //{
    //    GameWon();
    //}

    private void OnDungeonGenerated()
    {
        this.playerController.transform.position = new Vector3(this.dungeonController.StartCoordinate.X, this.dungeonController.StartCoordinate.Y, -1f);
    }
}