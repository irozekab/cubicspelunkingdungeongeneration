﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kameosa
{
    namespace Collections
    {
        namespace IList
        {
            public static class Util
            {
                /// <summary>
                /// Randomize the content of the list.
                /// </summary>
                /// <typeparam name="T"></typeparam>
                /// <param name="list"></param>
                public static void Randomize<T>(this System.Collections.Generic.IList<T> list)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        T temp = list[i];
                        int randomIndex = Random.Range(i, list.Count);
                        list[i] = list[randomIndex];
                        list[randomIndex] = temp;
                    }
                }

                public static void Fill<T>(this System.Collections.Generic.IList<T> list, int size, T value)
                {
                    list.Clear();

                    for (int i = 0; i < size; i++)
                    {
                        list.Add(value);
                    }
                }

                public static void Fill<T>(this System.Collections.Generic.IList<T> list, T value)
                {
                    list.Fill(list.Count, value);
                }

                /// <summary>
                /// Get a random element from the list.
                /// Returns default(T) if list is empty.
                /// </summary>
                /// <typeparam name="T"></typeparam>
                /// <param name="list"></param>
                /// <returns></returns>
                public static T GetRandom<T>(this System.Collections.Generic.IList<T> list)
                {
                    if (list.Count == 0)
                    {
                        return default(T);
                    }

                    return list[Random.Range(0, list.Count)];
                }

                /// <summary>
                /// Get the median of the list.
                /// Returns the lower median if there are even number of elements.
                /// Returns default(T) if list is empty.
                /// </summary>
                /// <typeparam name="T"></typeparam>
                /// <param name="list"></param>
                /// <returns></returns>
                public static T GetMedian<T>(this IList<T> list)
                {
                    if (list.Count == 0)
                    {
                        return default(T);
                    }

                    return list[Mathf.FloorToInt(list.Count / 2)];
                }
            }
        }
    }
}
