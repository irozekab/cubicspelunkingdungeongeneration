﻿using Kameosa.Enumerables;
using Kameosa.GameObject.Common.Controller;
using UnityEngine;

namespace Kameosa
{
    namespace GameObject
    {
        namespace Gameplay2D
        {
            namespace Common
            {
                namespace Joystick
                {
                    //[RequireComponent(typeof(Rigidbody2D), typeof(Animator), typeof(SpriteRenderer))]
                    //[RequireComponent(typeof(AudioSource))]
                    public abstract class BaseJoystickComponent : MonoBehaviour
                    {
                        [SerializeField]
                        protected new Rigidbody2D rigidbody2D;

                        [SerializeField]
                        protected Animator animator;

                        [SerializeField]
                        protected SpriteRenderer spriteRenderer;

                        [SerializeField]
                        protected AudioSource audioSource;

                        [SerializeField]
                        protected BaseGameplayController gameplayController;

                        protected bool canMove = true;

                        protected RelativeDirection faceDirection = RelativeDirection.Right;

                        #region Properties

                        public bool CanMove
                        {
                            get
                            {
                                return this.canMove;
                            }

                            set
                            {
                                this.canMove = value;
                            }
                        }

                        #endregion

                        protected virtual void Awake()
                        {
                            if (this.rigidbody2D == null)
                            {
                                this.rigidbody2D = GetComponent<Rigidbody2D>();
                            }

                            if (this.animator == null)
                            {
                                this.animator = GetComponent<Animator>();
                            }

                            if (this.spriteRenderer == null)
                            {
                                this.spriteRenderer = GetComponent<SpriteRenderer>();
                            }

                            if (this.audioSource == null)
                            {
                                this.audioSource = GetComponent<AudioSource>();
                            }

                            if (this.gameplayController == null)
                            {
                                this.gameplayController = UnityEngine.GameObject.Find("Gameplay").GetComponent<BaseGameplayController>();
                            }
                        }

                        protected virtual void Start()
                        {
                            Initialize();
                        }

                        protected abstract void Initialize();

                        //    protected virtual UnityEngine.Vector3 ClampPosition(UnityEngine.Vector3 position)
                        //    {
                        //        return Vector3.Util.Clamp(position, this.gameplayController.XBounds.First, this.gameplayController.XBounds.Second, this.gameplayController.YBounds.First, this.gameplayController.YBounds.Second);
                        //    }
                    }
                }
            }
        }
    }
}