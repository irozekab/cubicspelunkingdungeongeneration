﻿using System.Collections;
using System.Collections.Generic;
using Kameosa.GameObject.Common.Controller;
using UnityEngine;

namespace Kameosa
{
    namespace GameObject
    {
        namespace Gameplay2D
        {
            namespace Common
            {
                namespace Controller
                {
                    public struct FocusArea
                    {
                        public UnityEngine.Vector2 centre;
                        public UnityEngine.Vector2 velocity;
                        float left, right, top, bottom;

                        public FocusArea(Bounds bounds, UnityEngine.Vector2 size)
                        {
                            this.left = bounds.center.x - size.x / 2;
                            this.right = bounds.center.x + size.x / 2;
                            this.bottom = bounds.min.y;
                            this.top = bounds.min.y + size.y;

                            this.velocity = UnityEngine.Vector2.zero;
                            this.centre = new UnityEngine.Vector2((this.left + this.right) / 2, (this.top + this.bottom) / 2);
                        }

                        public void Update(Bounds bounds)
                        {
                            float shiftX = 0;

                            if (bounds.min.x < this.left)
                            {
                                shiftX = bounds.min.x - this.left;
                            }
                            else if (bounds.max.x > this.right)
                            {
                                shiftX = bounds.max.x - this.right;
                            }

                            this.left += shiftX;
                            this.right += shiftX;

                            float shiftY = 0;

                            if (bounds.min.y < this.bottom)
                            {
                                shiftY = bounds.min.y - this.bottom;
                            }
                            else if (bounds.max.y > this.top)
                            {
                                shiftY = bounds.max.y - this.top;
                            }

                            this.top += shiftY;
                            this.bottom += shiftY;

                            this.centre = new UnityEngine.Vector2((this.left + this.right) / 2, (this.top + this.bottom) / 2);
                            this.velocity = new UnityEngine.Vector2(shiftX, shiftY);
                        }
                    }

                    public class MainCameraController : MonoBehaviour
                    {
                        private const float STOP_LOOKAHEAD_MULTIPLIER = 0.25f;

                        [SerializeField]
                        private float xLookaheadDistance = 3f;

                        [SerializeField]
                        private float yOffset = 1f;

                        [SerializeField]
                        private float xLookaheadSmoothTime = 0.5f;

                        [SerializeField]
                        private float yOffsetSmoothTime = 0.2f;

                        [SerializeField]
                        private bool isFollowTargetPosition = true;

                        [SerializeField]
                        private UnityEngine.Vector2 focusAreaSize = new UnityEngine.Vector2(3f, 5f);

                        [SerializeField]
                        private FocusArea focusArea;

                        [SerializeField]
                        private Platformer.Joystick.RaycastPhysicsJoystickComponent target;
                        private Collider2D targetCollider;

                        [SerializeField]
                        private BaseGameplayController baseGameplayController;

                        private float currentXLookaheadDistance;
                        private float targetXLookaheadDistance;
                        private float directionX;
                        private float xLookaheadSmoothDampVelocityReference;
                        private float yOffsetSmoothDampVelocityReference;
                        private bool isLookaheadStop;

                        private void Awake()
                        {
                            if (this.baseGameplayController == null)
                            {
                                this.baseGameplayController = UnityEngine.GameObject.Find("Gameplay").GetComponent<BaseGameplayController>();
                            }

                            if (this.target == null)
                            {
                                UnityEngine.GameObject player = UnityEngine.GameObject.Find("Player") as UnityEngine.GameObject;

                                if (player != null)
                                {
                                    this.target = player.GetComponent<Platformer.Joystick.RaycastPhysicsJoystickComponent>();
                                }
                            }

                            if (this.target != null)
                            {
                                this.targetCollider = this.target.GetComponent<Collider2D>();
                            }
                        }

                        private void Start()
                        {
                            if (this.targetCollider == null)
                            {
                                this.isFollowTargetPosition = false;
                            }
                            else
                            {
                                this.focusArea = new FocusArea(this.targetCollider.bounds, this.focusAreaSize);
                            }
                        }

                        private void LateUpdate()
                        {
                            if (this.isFollowTargetPosition)
                            {
                                this.focusArea.Update(this.targetCollider.bounds);

                                UnityEngine.Vector2 focusPosition = this.focusArea.centre + UnityEngine.Vector2.up * this.yOffset;

                                if (this.focusArea.velocity.x != 0)
                                {
                                    this.directionX = Mathf.Sign(this.focusArea.velocity.x);

                                    // Only complete the lookahead if user input is still in the same direction as the lookahead. Else,a stop with a multiplier.
                                    if (Mathf.Sign(this.target.Input.x) == Mathf.Sign(this.directionX) && this.target.Input.x != 0)
                                    {
                                        this.isLookaheadStop = false;
                                        this.targetXLookaheadDistance = this.directionX * this.xLookaheadDistance;
                                    }
                                    else
                                    {
                                        if (!this.isLookaheadStop)
                                        {
                                            this.isLookaheadStop = true;
                                            this.targetXLookaheadDistance = this.currentXLookaheadDistance + ((this.directionX * this.xLookaheadDistance - this.currentXLookaheadDistance) * STOP_LOOKAHEAD_MULTIPLIER);
                                        }
                                    }
                                }

                                this.currentXLookaheadDistance = Mathf.SmoothDamp(this.currentXLookaheadDistance, this.targetXLookaheadDistance, ref this.xLookaheadSmoothDampVelocityReference, this.xLookaheadSmoothTime);

                                focusPosition.y = Mathf.SmoothDamp(this.transform.position.y, focusPosition.y, ref this.yOffsetSmoothDampVelocityReference, this.yOffsetSmoothTime);
                                focusPosition += UnityEngine.Vector2.right * this.currentXLookaheadDistance;

                                this.transform.position = (UnityEngine.Vector3) focusPosition + UnityEngine.Vector3.forward * -10;
                            }
                        }

                        private void OnDrawGizmos()
                        {
                            if (this.isFollowTargetPosition)
                            {
                                UnityEngine.Gizmos.color = new Color(1, 0, 0, 0.5f);
                                UnityEngine.Gizmos.DrawCube(this.focusArea.centre, this.focusAreaSize);
                            }
                        }
                    }
                }
            }
        }
    }
}
