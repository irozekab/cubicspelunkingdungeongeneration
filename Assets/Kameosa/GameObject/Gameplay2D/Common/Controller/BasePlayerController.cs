﻿using System;
using Kameosa.GameObject.Common.Controller;
using UnityEngine;

namespace Kameosa
{
    namespace GameObject
    {
        namespace Gameplay2D
        {
            namespace Common
            {
                namespace Controller
                {
                    [RequireComponent(typeof(Rigidbody2D), typeof(Animator), typeof(AudioSource))]
                    public class BasePlayerController : MonoBehaviour
                    {
                        [SerializeField]
                        protected new Rigidbody2D rigidbody2D;

                        [SerializeField]
                        protected Animator animator;

                        [SerializeField]
                        protected AudioSource audioSource;

                        [SerializeField]
                        protected BaseGameplayController baseGameplayController;

                        public event Action OnDie;

                        #region Properties

                        public float X
                        {
                            get
                            {
                                return this.transform.position.x;
                            }
                        }

                        public float Y
                        {
                            get
                            {
                                return this.transform.position.y;
                            }
                        }

                        #endregion

                        protected virtual void Awake()
                        {
                            if (this.rigidbody2D == null)
                            {
                                this.rigidbody2D = GetComponent<Rigidbody2D>();
                            }

                            if (this.animator == null)
                            {
                                this.animator = GetComponent<Animator>();
                            }

                            if (this.audioSource == null)
                            {
                                this.audioSource = GetComponent<AudioSource>();
                            }

                            if (this.baseGameplayController == null)
                            {
                                this.baseGameplayController = UnityEngine.GameObject.Find("Gameplay").GetComponent<BaseGameplayController>();
                            }
                        }

                        protected virtual void OnEnable()
                        {
                            this.baseGameplayController.OnGameAwake += OnGameAwake;
                            this.baseGameplayController.OnGameStart += OnGameStart;
                            this.baseGameplayController.OnGamePause += OnGamePause;
                            this.baseGameplayController.OnGameResume += OnGameResume;
                            this.baseGameplayController.OnGameWon += OnGameWon;
                            this.baseGameplayController.OnGameLose += OnGameLose;
                            this.baseGameplayController.OnGameEnd += OnGameEnd;
                            this.baseGameplayController.OnGameRestart += OnGameRestart;
                            this.baseGameplayController.OnGameQuit += OnGameQuit;
                        }

                        protected virtual void Start()
                        {
                        }

                        protected virtual void OnDisable()
                        {
                            this.baseGameplayController.OnGameAwake -= OnGameAwake;
                            this.baseGameplayController.OnGameStart -= OnGameStart;
                            this.baseGameplayController.OnGamePause -= OnGamePause;
                            this.baseGameplayController.OnGameResume -= OnGameResume;
                            this.baseGameplayController.OnGameWon -= OnGameWon;
                            this.baseGameplayController.OnGameLose -= OnGameLose;
                            this.baseGameplayController.OnGameEnd -= OnGameEnd;
                            this.baseGameplayController.OnGameRestart -= OnGameRestart;
                            this.baseGameplayController.OnGameQuit -= OnGameQuit;
                        }

                        protected virtual void OnCollisionEnter2D(Collision2D collision)
                        {
#if UNITY_EDITOR
                            Kameosa.Services.LogService.Info("PlayerController OnCollisionEnter2D()");
#endif
                        }

                        protected virtual void OnTriggerEnter2D(Collider2D collision)
                        {
#if UNITY_EDITOR
                            Kameosa.Services.LogService.Info("PlayerController OnTriggerEnter2D() collision.tag: " + collision.tag);
#endif
                        }

                        protected virtual void Die()
                        {
#if UNITY_EDITOR
                            Kameosa.Services.LogService.Info("PlayerController Die()");
#endif

                            if (OnDie != null)
                            {
                                OnDie();
                            }
                        }

                        protected virtual void OnGameAwake()
                        {
                        }

                        protected virtual void OnGameStart()
                        {
                        }

                        protected virtual void OnGamePause()
                        {
                        }

                        protected virtual void OnGameResume()
                        {
                        }

                        protected virtual void OnGameWon()
                        {
                        }

                        protected virtual void OnGameLose()
                        {
                        }

                        protected virtual void OnGameEnd()
                        {
                        }

                        protected virtual void OnGameRestart()
                        {
                        }

                        protected virtual void OnGameQuit()
                        {
                        }
                    }
                }
            }
        }
    }
}
