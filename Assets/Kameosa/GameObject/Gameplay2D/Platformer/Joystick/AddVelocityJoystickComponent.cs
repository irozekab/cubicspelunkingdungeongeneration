﻿using Kameosa.Enumerables;
using UnityEngine;

namespace Kameosa
{
    namespace GameObject
    {
        namespace Gameplay2D
        {
            namespace Platformer
            {
                namespace Joystick
                {
                    // https://unity3d.com/learn/tutorials/topics/2d-game-creation/2d-character-controllers?playlist=17120
                    public class AddVelocityJoystickComponent : Kameosa.GameObject.Gameplay2D.Common.Joystick.BaseJoystickComponent
                    {
                        protected ForceMode2D jumpForceMode = ForceMode2D.Impulse;

                        [SerializeField]
                        protected float notGroundedVelocityMultiplier = 0.8f;

                        [SerializeField]
                        protected bool isGrounded = true;

                        [SerializeField]
                        protected bool canJump = true;

                        [SerializeField]
                        protected float jumpForce = 7f;

                        [SerializeField]
                        protected float horizontalMaxVelocity = 7f;

                        [SerializeField]
                        protected bool isFlipXScaleOnLeft = false;

                        [SerializeField]
                        protected bool isSetAnimatorVerticalVelocity = false;

                        [SerializeField]
                        protected bool canMoveIfNotGrounded = true;

                        protected virtual void Update()
                        {
                            this.isGrounded = IsGrounded();

                            if (this.canJump)
                            {
                                CheckJump();
                            }

                            if (this.canMove && (this.canMoveIfNotGrounded || this.isGrounded))
                            {
                                Move();
                            }

                            if (this.isSetAnimatorVerticalVelocity)
                            {
                                this.animator.SetFloat("verticalVelocity", this.rigidbody2D.velocity.y);
                            }
                        }

                        protected override void Initialize()
                        {
                        }

                        protected virtual void Move()
                        {
                            float horizontalInput = Input.GetAxis("Horizontal");

                            float velocity = horizontalInput * this.horizontalMaxVelocity * (this.isGrounded ? 1f : this.notGroundedVelocityMultiplier);
                            this.rigidbody2D.velocity = new UnityEngine.Vector2(velocity, this.rigidbody2D.velocity.y);

                            this.animator.SetFloat("horizontalSpeed", Mathf.Abs(horizontalInput));

                            SetFaceDirection(horizontalInput);
                        }

                        protected virtual void SetFaceDirection(float horizontalInput)
                        {
                            if (horizontalInput == 0)
                            {
                                return;
                            }

                            this.faceDirection = horizontalInput > 0 ? RelativeDirection.Right : RelativeDirection.Left;

                            if (this.isFlipXScaleOnLeft)
                            {
                                this.spriteRenderer.flipX = this.faceDirection == RelativeDirection.Left;
                            }
                        }

                        public virtual void CheckJump()
                        {
                            if (this.canJump && this.isGrounded && Input.GetButtonDown("Jump"))
                            {
                                this.rigidbody2D.AddForce(new UnityEngine.Vector2(0f, this.jumpForce), this.jumpForceMode);
                            }
                        }

                        public bool IsGrounded()
                        {
                            return (Math.Util.IsEqual(Mathf.Abs(this.rigidbody2D.velocity.y), 0f));
                        }
                    }
                }
            }
        }
    }
}