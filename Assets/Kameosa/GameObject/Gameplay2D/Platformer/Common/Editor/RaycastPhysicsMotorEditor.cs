﻿using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEditor;
using Kameosa.GameObject.Gameplay2D.Platformer.Common;
using Kameosa.Services;

[CustomEditor(typeof(RaycastPhysicsMotorComponent))]
[CanEditMultipleObjects]
public class RaycastPhysicsMotorComponentEditor : Editor
{
    private const float MINIMUM_DISTANCE_CHECK = 0.01f;

    private Dictionary<string, SerializedProperty> properties = new Dictionary<string, SerializedProperty>();
    private List<Property> timingProperties = new List<Property>();

    private class Property
    {
        public string name;
        public string text;

        public Property(string name, string text)
        {
            this.name = name;
            this.text = text;
        }
    }

    private static bool showGeneral;
    private static bool showMovement;
    private static bool showSlopes;
    private static bool showJumping;
    private static bool showWallInteractions;
    private static bool showDashing;
    private static bool showInformation;

    #region Properties

    private readonly Property STATIC_ENV_LAYER_MASK = new Property("staticEnvLayerMask", "Static Environment Layer Mask");
    private readonly Property ENV_CHECK_DISTANCE = new Property("envCheckDistance", "Environment Check Distance");
    private readonly Property MIN_DISTANCE_FROM_ENV = new Property("minDistanceFromEnv", "Minimum Distance from Environment");
    private readonly Property NUM_OF_ITERATIONS = new Property("numOfIterations", "Number of Iterations");
    private readonly Property ENABLE_ONE_WAY_PLATFORMS = new Property("enableOneWayPlatforms", "Enable One Way Platforms");
    private readonly Property MOVING_PLATFORM_LAYER_MASK = new Property("movingPlatformLayerMask", "Moving Platforms Layer Mask");
    private readonly Property RAYCASTS_PER_SIDE = new Property("additionalRaycastsPerSide", "Additional Raycasts per Side");

    private readonly Property GROUND_SPEED = new Property("groundSpeed", "Ground Speed");
    private readonly Property TIME_TO_GROUND_SPEED = new Property("timeToGroundSpeed", "Time to Ground Speed");
    private readonly Property GROUND_STOP_DISTANCE = new Property("groundStopDistance", "Ground Stop Distance");

    private readonly Property AIR_SPEED = new Property("airSpeed", "Air Speed");
    private readonly Property CHANGE_DIR_IN_AIR = new Property("canChangeDirectionInAir", "Enable Change Direction in Air");
    private readonly Property TIME_TO_AIR_SPEED = new Property("timeToAirSpeed", "Time to Air Speed");
    private readonly Property AIR_STOP_DISTANCE = new Property("airStopDistance", "Air Stop Distance");

    private readonly Property FALL_SPEED = new Property("fallSpeed", "Fall Speed");
    private readonly Property GRAVITY_MUTLIPLIER = new Property("gravityMultiplier", "Fall Gravity Multiplier");
    private readonly Property FAST_FALL_SPEED = new Property("fastFallSpeed", "Fast Fall Speed");

    private readonly Property FAST_FALL_GRAVITY_MULTIPLIER = new Property("fastFallGravityMultiplier", "Fast Fall Gravity Multiplier");

    private readonly Property LADDER_SPEED = new Property("ladderSpeed", "Ladder Speed");

    private readonly Property ENABLE_SLOPES = new Property("enableSlopes", "Enable Slopes");

    private readonly Property ANGLE_ALLOWED_FOR_SLOPES = new Property("angleAllowedForMoving", "Angle (Degrees) Allowed For Moving");

    private readonly Property CHANGE_SPEED_ON_SLOPES = new Property("enableChangeSpeedOnSlopes", "Change Speed on Slopes");
    private readonly Property MIN_SPEED_TO_MOVE_UP_SLIPPERY_SLOPE = new Property("minimumSpeedToMoveUpSlipperySlope", "Minimum Speed to Move Up Slippery Slope");

    private readonly Property SLOPES_SPEED_MULTIPLIER = new Property("speedMultiplierOnSlope", "Speed Multiplier on Slopes");
    private readonly Property SLOPE_NORMAL = new Property("slopeNormal", "Touching slope angle");
    private readonly Property STICK_TO_GROUND = new Property("enableStickOnGround", "Stick to Ground");
    private readonly Property STICK_CHECK_DISTANCE = new Property("distanceToCheckToStick", "Ground Check Distance to Stick");

    private readonly Property JUMP_HEIGHT = new Property("jumpHeight", "Jump Height");
    private readonly Property EXTRA_JUMP_HEIGHT = new Property("extraJumpHeight", "Extra Jump Height");
    private readonly Property NUM_OF_AIR_JUMPS = new Property("numOfAirJumps", "Number of Air Jumps");
    private readonly Property JUMP_WINDOW_WHEN_FALLING = new Property("jumpWindowWhenFalling", "Jump Window When Falling");
    private readonly Property JUMP_WINDOW_WHEN_ACTIVATED = new Property("jumpWindowWhenActivated", "Jump Window When Activated");

    private readonly Property ENABLE_WALL_JUMPS = new Property("enableWallJumps", "Enable Wall Jumps");
    private readonly Property WALL_JUMP_MULTIPLIER = new Property("wallJumpMultiplier", "Wall Jump Multiplier");
    private readonly Property WALL_JUMP_DEGREE = new Property("wallJumpAngle", "Wall Jump Angle (Degrees)");
    private readonly Property WALL_JUMP_AWAY_DEGREE = new Property("wallJumpAwayAngle", "Wall Jump Away Angle (Degrees)");

    private readonly Property ENABLE_WALL_STICKS = new Property("enableWallSticks", "Enable Wall Sticks");
    private readonly Property WALL_STICK_DURATION = new Property("wallSticksDuration", "Wall Stick Duration");

    private readonly Property ENABLE_WALL_SLIDES = new Property("enableWallSlides", "Enable Wall Slides");
    private readonly Property WALL_SLIDE_SPEED = new Property("wallSlideSpeed", "Wall Slide Speed");
    private readonly Property TIME_TO_WALL_SLIDE_SPEED = new Property("timeToWallSlideSpeed", "Time to Wall Slide Speed");

    private readonly Property ENABLE_CORNER_GRABS = new Property("enableCornerGrabs", "Enable Corner Grabs");
    private readonly Property CORNER_JUMP_MULTIPLIER = new Property("cornerJumpMultiplier", "Corner Jump Multiplier");
    private readonly Property CORNER_GRAB_DURATION = new Property("cornerGrabDuration", "Corner Grab Duration");
    private readonly Property CORNER_DISTANCE_CHECK = new Property("cornerDistanceCheck", "Distance Check for Corner Grab");
    private readonly Property NORMALIZED_VALID_WALL_INTERACTION = new Property("normalizedValidWallInteraction", "Valid Normalized Interaction Area");

    private readonly Property WALL_INTERACTION_IGNORE_MOVEMENT_DURATION = new Property("ignoreMovementAfterJump", "Ignore Movement After Jump Duration");

    private readonly Property WALL_INTERACTION_COOLDOWN = new Property("wallInteractionCooldown", "Wall Interaction Cooldown");
    private readonly Property WALL_INTERACTION_THRESHOLD = new Property("wallInteractionThreshold", "Wall Interaction Threshold");

    private readonly Property ENABLE_DASHES = new Property("enableDashes", "Enable Dashes");
    private readonly Property DASH_DISTANCE = new Property("dashDistance", "Dash Distance");
    private readonly Property DASH_EASING_FUNCTION = new Property("dashEasingFunction", "Dash Easing Function");
    private readonly Property DASH_DURATION = new Property("dashDuration", "Dash Duration");
    private readonly Property DASH_COOLDOWN = new Property("dashCooldown", "Dash Cooldown");
    private readonly Property END_DASH_DELAY = new Property("endDashNoGravityDuration", "Gravity Delay After Dash");

    #endregion

    private void OnEnable()
    {
        this.properties.Clear();
        SerializedProperty property = serializedObject.GetIterator();

        while (property.NextVisible(true))
        {
            this.properties[property.name] = property.Copy();
        }
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        this.timingProperties.Clear();

        GUIStyle boldStyle = new GUIStyle
        {
            fontStyle = FontStyle.Bold
        };

        EditorGUILayout.Separator();

        RaycastPhysicsMotorComponentEditor.showGeneral = EditorGUILayout.Foldout(RaycastPhysicsMotorComponentEditor.showGeneral, "General");

        if (RaycastPhysicsMotorComponentEditor.showGeneral)
        {
            DisplayRegularField(this.STATIC_ENV_LAYER_MASK);
            DisplayRegularField(this.ENV_CHECK_DISTANCE);
            DisplayRegularField(this.MIN_DISTANCE_FROM_ENV);
            DisplayRegularField(this.NUM_OF_ITERATIONS);
            DisplayRegularField(this.ENABLE_ONE_WAY_PLATFORMS);

            EditorGUILayout.Separator();

            DisplayRegularField(this.MOVING_PLATFORM_LAYER_MASK);

            if (this.properties[this.MOVING_PLATFORM_LAYER_MASK.name].hasMultipleDifferentValues ||
                this.properties[this.MOVING_PLATFORM_LAYER_MASK.name].intValue != 0)
            {
                DisplayRegularField(this.RAYCASTS_PER_SIDE);
            }

            EditorGUILayout.Separator();
        }

        RaycastPhysicsMotorComponentEditor.showMovement = EditorGUILayout.Foldout(RaycastPhysicsMotorComponentEditor.showMovement, "Movement");

        if (RaycastPhysicsMotorComponentEditor.showMovement)
        {
            DisplayRateField(this.GROUND_SPEED);
            DisplayAccelerationField(this.TIME_TO_GROUND_SPEED);
            DisplayRegularField(this.GROUND_STOP_DISTANCE);

            EditorGUILayout.Separator();


            DisplayRateField(this.AIR_SPEED);
            DisplayRegularField(this.CHANGE_DIR_IN_AIR);

            if (this.properties[this.CHANGE_DIR_IN_AIR.name].hasMultipleDifferentValues || this.properties[this.CHANGE_DIR_IN_AIR.name].boolValue)
            {
                DisplayAccelerationField(this.TIME_TO_AIR_SPEED);
                DisplayRegularField(this.AIR_STOP_DISTANCE);
            }

            EditorGUILayout.Separator();

            DisplayRateField(this.FALL_SPEED);
            DisplayRegularField(this.GRAVITY_MUTLIPLIER);
            DisplayRateField(this.FAST_FALL_SPEED);
            DisplayRegularField(this.FAST_FALL_GRAVITY_MULTIPLIER);

            EditorGUILayout.Separator();

            DisplayRegularField(this.LADDER_SPEED);

            EditorGUILayout.Separator();
        }

        RaycastPhysicsMotorComponentEditor.showJumping = EditorGUILayout.Foldout(RaycastPhysicsMotorComponentEditor.showJumping, "Jumping");

        if (RaycastPhysicsMotorComponentEditor.showJumping)
        {
            DisplayRegularField(this.JUMP_HEIGHT);
            DisplayRegularField(this.EXTRA_JUMP_HEIGHT);
            DisplayRegularField(this.NUM_OF_AIR_JUMPS);
            DisplayTimingField(this.JUMP_WINDOW_WHEN_FALLING);
            DisplayTimingField(this.JUMP_WINDOW_WHEN_ACTIVATED);

            EditorGUILayout.Separator();
        }

        RaycastPhysicsMotorComponentEditor.showSlopes = EditorGUILayout.Foldout(RaycastPhysicsMotorComponentEditor.showSlopes, "Slopes");

        if (RaycastPhysicsMotorComponentEditor.showSlopes)
        {
            DisplayRegularField(this.ENABLE_SLOPES);

            if (this.properties[this.ENABLE_SLOPES.name].hasMultipleDifferentValues || this.properties[this.ENABLE_SLOPES.name].boolValue)
            {
                DisplayRegularField(this.ANGLE_ALLOWED_FOR_SLOPES);
                DisplayRegularField(this.MIN_SPEED_TO_MOVE_UP_SLIPPERY_SLOPE);
                DisplayRegularField(this.CHANGE_SPEED_ON_SLOPES);

                if (this.properties[this.CHANGE_SPEED_ON_SLOPES.name].hasMultipleDifferentValues ||
                    this.properties[this.CHANGE_SPEED_ON_SLOPES.name].boolValue)
                {
                    DisplayRegularField(this.SLOPES_SPEED_MULTIPLIER);
                }

                DisplayRegularField(this.STICK_TO_GROUND);

                if (this.properties[this.STICK_TO_GROUND.name].hasMultipleDifferentValues || this.properties[this.STICK_TO_GROUND.name].boolValue)
                {
                    DisplayRegularField(this.STICK_CHECK_DISTANCE);
                }
            }

            EditorGUILayout.Separator();
        }

        RaycastPhysicsMotorComponentEditor.showWallInteractions = EditorGUILayout.Foldout(RaycastPhysicsMotorComponentEditor.showWallInteractions, "Wall Interactions");

        if (RaycastPhysicsMotorComponentEditor.showWallInteractions)
        {
            DisplayRegularField(this.ENABLE_WALL_JUMPS);

            if (this.properties[this.ENABLE_WALL_JUMPS.name].hasMultipleDifferentValues || this.properties[this.ENABLE_WALL_JUMPS.name].boolValue)
            {
                DisplayRegularField(this.WALL_JUMP_MULTIPLIER);
                DisplayRegularField(this.WALL_JUMP_DEGREE);
                DisplayRegularField(this.WALL_JUMP_AWAY_DEGREE);
            }

            EditorGUILayout.Separator();

            DisplayRegularField(this.ENABLE_WALL_STICKS);

            if (this.properties[this.ENABLE_WALL_STICKS.name].hasMultipleDifferentValues || this.properties[this.ENABLE_WALL_STICKS.name].boolValue)
            {
                DisplayTimingField(this.WALL_STICK_DURATION);
            }

            EditorGUILayout.Separator();

            DisplayRegularField(this.ENABLE_WALL_SLIDES);

            if (this.properties[this.ENABLE_WALL_SLIDES.name].hasMultipleDifferentValues || this.properties[this.ENABLE_WALL_SLIDES.name].boolValue)
            {
                DisplayRateField(this.WALL_SLIDE_SPEED);
                DisplayAccelerationField(this.TIME_TO_WALL_SLIDE_SPEED);
            }

            EditorGUILayout.Separator();

            DisplayRegularField(this.ENABLE_CORNER_GRABS);

            if (this.properties[this.ENABLE_CORNER_GRABS.name].hasMultipleDifferentValues || this.properties[this.ENABLE_CORNER_GRABS.name].boolValue)
            {
                DisplayTimingField(this.CORNER_GRAB_DURATION);
                DisplayRegularField(this.CORNER_JUMP_MULTIPLIER);
                DisplayRegularField(this.CORNER_DISTANCE_CHECK);
            }

            EditorGUILayout.Separator();

            if ((this.properties[this.ENABLE_WALL_JUMPS.name].hasMultipleDifferentValues ||
                    this.properties[this.ENABLE_WALL_JUMPS.name].boolValue) ||
                (this.properties[this.ENABLE_CORNER_GRABS.name].hasMultipleDifferentValues ||
                    this.properties[this.ENABLE_CORNER_GRABS.name].boolValue))
            {
                DisplayTimingField(this.WALL_INTERACTION_IGNORE_MOVEMENT_DURATION);
            }

            EditorGUILayout.Separator();

            if ((this.properties[this.ENABLE_WALL_STICKS.name].hasMultipleDifferentValues ||
                    this.properties[this.ENABLE_WALL_STICKS.name].boolValue) ||
                (this.properties[this.ENABLE_CORNER_GRABS.name].hasMultipleDifferentValues ||
                    this.properties[this.ENABLE_CORNER_GRABS.name].boolValue) ||
                (this.properties[this.ENABLE_WALL_SLIDES.name].hasMultipleDifferentValues ||
                    this.properties[this.ENABLE_WALL_SLIDES.name].boolValue))
            {
                DisplayRegularField(this.NORMALIZED_VALID_WALL_INTERACTION);
                DisplayTimingField(this.WALL_INTERACTION_COOLDOWN);
                DisplayRegularField(this.WALL_INTERACTION_THRESHOLD);
            }

            EditorGUILayout.Separator();
        }

        RaycastPhysicsMotorComponentEditor.showDashing = EditorGUILayout.Foldout(RaycastPhysicsMotorComponentEditor.showDashing, "Dashing");

        if (RaycastPhysicsMotorComponentEditor.showDashing)
        {
            DisplayRegularField(this.ENABLE_DASHES);

            if (this.properties[this.ENABLE_DASHES.name].hasMultipleDifferentValues || this.properties[this.ENABLE_DASHES.name].boolValue)
            {
                DisplayRegularField(this.DASH_DISTANCE);

                DisplayTimingField(this.DASH_DURATION);
                DisplayTimingField(this.DASH_COOLDOWN);

                DisplayRegularField(this.DASH_EASING_FUNCTION);
                DisplayTimingField(this.END_DASH_DELAY);
            }

            EditorGUILayout.Separator();
        }

        if (!serializedObject.isEditingMultipleObjects)
        {
            RaycastPhysicsMotorComponentEditor.showInformation = EditorGUILayout.Foldout(RaycastPhysicsMotorComponentEditor.showInformation, "Information");

            if (RaycastPhysicsMotorComponentEditor.showInformation)
            {
                EditorGUILayout.HelpBox(
                    GetInformation(),
                    MessageType.Info,
                    true);
            }
        }

        CheckValues();

        CheckAndDisplayInfo();

        serializedObject.ApplyModifiedProperties();
    }

    private void CheckAndDisplayInfo()
    {
        if (!Physics2D.queriesStartInColliders &&
            (this.properties[this.MOVING_PLATFORM_LAYER_MASK.name].hasMultipleDifferentValues ||
            this.properties[this.MOVING_PLATFORM_LAYER_MASK.name].intValue != 0))
        {
            EditorGUILayout.HelpBox(
                "'Raycasts Start in Colliders' in the Physics 2D settings needs to be checked on for moving platforms",
                MessageType.Error,
                true);
        }

        if (!this.properties[this.STATIC_ENV_LAYER_MASK.name].hasMultipleDifferentValues &&
            this.properties[this.STATIC_ENV_LAYER_MASK.name].intValue == 0)
        {
            EditorGUILayout.HelpBox(
                "Static Environment Layer Mask is required to be set!",
                MessageType.Error,
                true);
        }

        if (!this.properties[this.STATIC_ENV_LAYER_MASK.name].hasMultipleDifferentValues &&
            (this.properties[this.STATIC_ENV_LAYER_MASK.name].intValue & (1 << ((RaycastPhysicsMotorComponent)target).gameObject.layer)) != 0)
        {
            EditorGUILayout.HelpBox(
                "The Static Environment Layer Mask can not include the layer the motor is on!",
                MessageType.Error,
                true);
        }

        for (int i = 0; i < this.timingProperties.Count; i++)
        {
            CheckAndDisplayTimingWarnings(this.timingProperties[i]);
        }
    }


    private void CheckAndDisplayTimingWarnings(Property property)
    {
        if (!this.properties[property.name].hasMultipleDifferentValues &&
            !Mathf.Approximately(this.properties[property.name].floatValue / Time.fixedDeltaTime,
                Mathf.Round(this.properties[property.name].floatValue / Time.fixedDeltaTime)))
        {
            string msg = string.Format(
                "'{0}' is not a multiple of the fixed time step ({1}). This results in an extra frame effectively making '{0}' {2} instead of {3}",
                property.text,
                Time.fixedDeltaTime,
                FrameService.GetFrameCount(this.properties[property.name].floatValue) * Time.fixedDeltaTime,
                this.properties[property.name].floatValue);

            EditorGUILayout.HelpBox(
                msg,
                MessageType.Warning,
                true);
        }
    }

    private void DisplayRegularField(Property property)
    {
        EditorGUILayout.PropertyField(this.properties[property.name], new GUIContent(property.text));
    }

    private void DisplayRateField(Property property)
    {
        string frameRate = "-";

        if (!this.properties[property.name].hasMultipleDifferentValues)
        {
            frameRate = (this.properties[property.name].floatValue * Time.fixedDeltaTime).ToString();
        }

        EditorGUILayout.PropertyField(this.properties[property.name],
            new GUIContent(string.Format("{0} ({1} Distance/Frame)", property.text, frameRate)));
    }

    private void DisplayTimingField(Property property)
    {
        this.timingProperties.Add(property);

        string frameCount = "-";

        if (!this.properties[property.name].hasMultipleDifferentValues)
        {
            frameCount = FrameService.GetFrameCount(this.properties[property.name].floatValue).ToString();
        }

        EditorGUILayout.PropertyField(this.properties[property.name],
            new GUIContent(string.Format("{0} ({1} Frames)", property.text, frameCount)));
    }

    private void DisplayAccelerationField(Property property)
    {
        string frameCount = "-";

        if (!this.properties[property.name].hasMultipleDifferentValues)
        {
            frameCount = (this.properties[property.name].floatValue / Time.fixedDeltaTime).ToString();
        }

        EditorGUILayout.PropertyField(this.properties[property.name],
            new GUIContent(string.Format("{0} ({1} Frames)", property.text, frameCount)));
    }

    private void CheckValues()
    {
        if (!this.properties[this.ENV_CHECK_DISTANCE.name].hasMultipleDifferentValues &&
            this.properties[this.ENV_CHECK_DISTANCE.name].floatValue <= MINIMUM_DISTANCE_CHECK * 2)
        {
            this.properties[this.ENV_CHECK_DISTANCE.name].floatValue = MINIMUM_DISTANCE_CHECK * 2;
        }

        if (!this.properties[this.MIN_DISTANCE_FROM_ENV.name].hasMultipleDifferentValues &&
            this.properties[this.MIN_DISTANCE_FROM_ENV.name].floatValue <= MINIMUM_DISTANCE_CHECK)
        {
            this.properties[this.MIN_DISTANCE_FROM_ENV.name].floatValue = MINIMUM_DISTANCE_CHECK;
        }

        if (!this.properties[this.NUM_OF_ITERATIONS.name].hasMultipleDifferentValues &&
            this.properties[this.NUM_OF_ITERATIONS.name].intValue < 0)
        {
            this.properties[this.NUM_OF_ITERATIONS.name].intValue = 1;
        }
    }

    private string GetInformation()
    {
        StringBuilder sb = new StringBuilder();

        sb.AppendFormat("Approx jump distance: {0}", GetJumpDistance());

        if (this.properties[this.TIME_TO_GROUND_SPEED.name].floatValue != 0)
        {
            sb.AppendFormat(
                "\nGround acceleration: {0}",
                this.properties[this.GROUND_SPEED.name].floatValue / this.properties[this.TIME_TO_GROUND_SPEED.name].floatValue);
        }

        if (this.properties[this.GROUND_STOP_DISTANCE.name].floatValue != 0)
        {
            sb.AppendFormat(
                "\nTime to stop on ground: {0}",
                GetTimeToDistance(
                    this.properties[this.GROUND_STOP_DISTANCE.name].floatValue,
                    this.properties[this.GROUND_SPEED.name].floatValue));
        }

        if (this.properties[this.TIME_TO_AIR_SPEED.name].floatValue != 0 && this.properties[this.CHANGE_DIR_IN_AIR.name].boolValue)
        {
            sb.AppendFormat(
                "\nMax air acceleration: {0}",
                this.properties[this.AIR_SPEED.name].floatValue / this.properties[this.TIME_TO_AIR_SPEED.name].floatValue);
        }

        if (this.properties[this.AIR_STOP_DISTANCE.name].floatValue != 0 && this.properties[this.CHANGE_DIR_IN_AIR.name].boolValue)
        {
            sb.AppendFormat(
                "\nTime to stop on ground: {0}",
                GetTimeToDistance(
                    this.properties[this.AIR_STOP_DISTANCE.name].floatValue,
                    this.properties[this.AIR_SPEED.name].floatValue));
        }

        sb.AppendFormat("\nApprox single jump duration (up & down): {0}",
            Mathf.Sqrt(-8 * (this.properties[this.JUMP_HEIGHT.name].floatValue + this.properties[this.EXTRA_JUMP_HEIGHT.name].floatValue) /
                (this.properties[this.GRAVITY_MUTLIPLIER.name].floatValue * Physics2D.gravity.y)));

        sb.AppendFormat(
            "\nWill hit fall speed cap during jump: {0}",
            (GetTotalJumpSpeed() >= this.properties[this.FALL_SPEED.name].floatValue));

        sb.AppendFormat(
            "\nFrames needed to get reach extra jump height: {0}",
            (this.properties[this.EXTRA_JUMP_HEIGHT.name].floatValue / GetBaseJumpSpeed()) / Time.fixedDeltaTime);

        if (this.properties[this.TIME_TO_WALL_SLIDE_SPEED.name].floatValue != 0 && this.properties[this.ENABLE_WALL_SLIDES.name].boolValue)
        {
            sb.AppendFormat(
                "\nWall slide acceleration: {0}",
                this.properties[this.WALL_SLIDE_SPEED.name].floatValue / this.properties[this.TIME_TO_WALL_SLIDE_SPEED.name].floatValue);
        }

        if (this.properties[this.ENABLE_SLOPES.name].boolValue)
        {
            sb.AppendFormat(
                "\nColliding slope angle: {0}",
                (Vector2.Angle(this.properties[this.SLOPE_NORMAL.name].vector2Value, Vector2.up)));
        }
        
        return sb.ToString();
    }

    private float GetJumpDistance()
    {
        return this.properties[this.AIR_SPEED.name].floatValue * 2 *
               Mathf.Sqrt(2 *
                    (this.properties[this.JUMP_HEIGHT.name].floatValue +
                    this.properties[this.EXTRA_JUMP_HEIGHT.name].floatValue) /
                    (((RaycastPhysicsMotorComponent)target).gravityMultiplier *
                    Mathf.Abs(Physics2D.gravity.y)));
    }

    private float GetBaseJumpSpeed()
    {
        return Mathf.Sqrt(-2 * ((this.properties[this.JUMP_HEIGHT.name].floatValue) *
            this.properties[this.GRAVITY_MUTLIPLIER.name].floatValue * Physics2D.gravity.y));
    }

    private float GetTotalJumpSpeed()
    {
        return Mathf.Sqrt(-2 * ((this.properties[this.JUMP_HEIGHT.name].floatValue + this.properties[this.EXTRA_JUMP_HEIGHT.name].floatValue) *
            this.properties[this.GRAVITY_MUTLIPLIER.name].floatValue * Physics2D.gravity.y));
    }

    private float GetTimeToDistance(float distance, float maxSpeed)
    {
        float deceleration = (maxSpeed * maxSpeed) / (2 * distance);

        return Mathf.Sqrt(2 * distance / deceleration);
    }
}
