﻿using System;
using System.Collections.Generic;
using Kameosa.GameObject.Gameplay2D.Platformer.Common;
using UnityEngine;

namespace Kameosa
{
    namespace GameObject
    {
        namespace Gameplay2D
        {
            namespace Platformer
            {
                namespace Patrol
                {
                    public class RaycastPhysicsMovingPlatformMotorComponent : MonoBehaviour
                    {
                        // Storage for frame to frame platform movement information.
                        [System.Serializable]
                        public struct PlatformMovement
                        {
                            public float velocity;
                            public bool isCyclic;
                            public float waitTimeAtWaypoint;
                            [Range(1, 3)]
                            public float easingValue;

                            [HideInInspector]
                            public int lastWaypointIndex;

                            [Range(0, 1)]
                            [HideInInspector]
                            public float percentageMoved;

                            [HideInInspector]
                            public float nextMoveTime;
                        }

                        #region Public Variables

                        public Action<RaycastPhysicsMotorComponent> onPlatformerMotorContact;

                        #endregion

                        #region Private Variables

                        [SerializeField]
                        private PlatformMovement platformMovement;

                        [SerializeField]
                        private List<UnityEngine.Vector2> localWaypoints;
                        private List<UnityEngine.Vector2> globalWaypoints;

                        private UnityEngine.Vector2 velocity;
                        private bool isVelocitySet;
                        private UnityEngine.Vector2 previousPosition;

                        #endregion

                        #region Properties

                        public UnityEngine.Vector2 Velocity
                        {
                            get
                            {
                                return this.velocity;
                            }
                            set
                            {
                                this.isVelocitySet = true;
                                this.velocity = value;
                            }
                        }

                        public UnityEngine.Vector2 Position
                        {
                            get { return this.transform.position; }
                            set
                            {
                                this.previousPosition = this.transform.position;
                                this.transform.position = value;
                                Velocity = UnityEngine.Vector2.zero;
                                this.isVelocitySet = false;
                            }
                        }

                        public UnityEngine.Vector2 PreviousPosition
                        {
                            get
                            {
                                return this.previousPosition;
                            }
                        }

                        #endregion

                        private void Start()
                        {
                            ConvertLocalToGlobalWaypoints();
                        }

                        //private void FixedUpdate()
                        //{
                        //    if (this.isVelocitySet)
                        //    {
                        //        this.previousPosition = this.transform.position;
                        //        this.transform.position += (UnityEngine.Vector3)Velocity * Time.fixedDeltaTime;
                        //    }
                        //}

                        private void Update()
                        {
                            if (this.isVelocitySet)
                            {
                                this.previousPosition = this.transform.position;
                                this.transform.position += (UnityEngine.Vector3)Velocity * Time.fixedDeltaTime;
                            }

                            UnityEngine.Vector2 velocity = this.globalWaypoints.Count > 1 ? CalculatePlatformMovement() : UnityEngine.Vector2.zero;
                            Move(velocity);
                        }

                        public void Move(UnityEngine.Vector2 velocity)
                        {
                            this.transform.Translate(velocity);
                        }

                        private void ConvertLocalToGlobalWaypoints()
                        {
                            this.globalWaypoints = new List<UnityEngine.Vector2>();

                            foreach (UnityEngine.Vector2 localWaypoint in this.localWaypoints)
                            {
                                this.globalWaypoints.Add(localWaypoint + (UnityEngine.Vector2)this.transform.position);
                            }
                        }

                        //https://youtu.be/3D0PeJh6GY8
                        private UnityEngine.Vector2 CalculatePlatformMovement()
                        {
                            // Do not move if it's not time to move yet.
                            if (Time.time < this.platformMovement.nextMoveTime)
                            {
                                return UnityEngine.Vector2.zero;
                            }

                            this.platformMovement.lastWaypointIndex %= this.globalWaypoints.Count;
                            int nextWaypointIndex = (this.platformMovement.lastWaypointIndex + 1) % this.globalWaypoints.Count;
                            float distanceBetweenWaypoints = UnityEngine.Vector2.Distance(this.globalWaypoints[this.platformMovement.lastWaypointIndex], this.globalWaypoints[nextWaypointIndex]);
                            this.platformMovement.percentageMoved += (Time.deltaTime * this.platformMovement.velocity) / distanceBetweenWaypoints;

                            // This will clamp the percentageMoved to [0, 1], because that's required for the function to work, and ease the value based on the the easingValue.
                            float easedPercentageMoved = Kameosa.Math.Util.Ease01(this.platformMovement.percentageMoved, this.platformMovement.easingValue);

                            // Use the eased value instead of the actual.
                            UnityEngine.Vector2 newPosition = UnityEngine.Vector2.Lerp(this.globalWaypoints[this.platformMovement.lastWaypointIndex], this.globalWaypoints[nextWaypointIndex], easedPercentageMoved);

                            // If we reach the next waypoint.
                            if (this.platformMovement.percentageMoved >= 1)
                            {
                                this.platformMovement.percentageMoved = 0;
                                this.platformMovement.lastWaypointIndex++;

                                if (!this.platformMovement.isCyclic)
                                {
                                    // If we reach the last waypoint, we want to restart from index 0, but we will reverse the globalwaypoints so that we move the platform backwards.
                                    if (this.platformMovement.lastWaypointIndex >= this.globalWaypoints.Count - 1)
                                    {
                                        this.platformMovement.lastWaypointIndex = 0;
                                        this.globalWaypoints.Reverse();
                                    }
                                }

                                // Set the next move time.
                                this.platformMovement.nextMoveTime = Time.time + this.platformMovement.waitTimeAtWaypoint;
                            }

                            return newPosition - (UnityEngine.Vector2)this.transform.position;
                        }

                        private void OnDrawGizmos()
                        {
                            if (!Application.isPlaying)
                            {
                                ConvertLocalToGlobalWaypoints();
                            }

                            Gizmos.Util.DrawConnectPoints(this.globalWaypoints, Color.red);
                        }
                    }
                }
            }
        }
    }
}
