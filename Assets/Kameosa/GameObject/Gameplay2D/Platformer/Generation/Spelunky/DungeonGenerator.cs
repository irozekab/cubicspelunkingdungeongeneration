﻿using Kameosa.Cartesian;
using Kameosa.Collections.IList;
using Kameosa.Services;
using UnityEngine;

namespace Kameosa
{
    namespace GameObject
    {
        namespace Gameplay2D
        {
            namespace Platformer
            {
                namespace Generation
                {
                    namespace Spelunky
                    {
                        public static class DungeonGenerator
                        {
                            private static bool isInit = false;
                            private static int dungeonHeightInRooms;
                            private static int dungeonWidthInRooms;
                            private static int roomHeightInTiles;
                            private static int roomWidthInTiles;
                            private static string templatesFilename;
                            private static Dungeon dungeon;
                            private static Templates templates;

                            public static void Init(int dungeonHeightInRooms = 4, int dungeonWidthInRooms = 4, int roomHeightInTiles = 8, int roomWidthInTiles = 10, string templatesFilename = "RoomTemplates")
                            {
                                UnityEngine.Random.InitState((int)System.DateTime.Now.Ticks);

                                DungeonGenerator.isInit = true;
                                DungeonGenerator.dungeonWidthInRooms = dungeonWidthInRooms;
                                DungeonGenerator.dungeonHeightInRooms = dungeonHeightInRooms;
                                DungeonGenerator.roomHeightInTiles = roomHeightInTiles;
                                DungeonGenerator.roomWidthInTiles = roomWidthInTiles;
                                DungeonGenerator.templatesFilename = templatesFilename;
                                DungeonGenerator.dungeon = new Dungeon(dungeonHeightInRooms, dungeonWidthInRooms, roomHeightInTiles, roomWidthInTiles);
                                DungeonGenerator.templates = FileService.JsonLoad<Templates>(Application.streamingAssetsPath, DungeonGenerator.templatesFilename);
                            }

                            // http://tinysubversions.com/spelunkyGen/
                            public static Dungeon Generate()
                            {
#if UNITY_EDITOR
                                if (!DungeonGenerator.isInit)
                                {
                                    Debug.LogWarning("DungeonGenerator is not initialize. Please run DungeonGenerator.Init() before Generate(). Proceeding with default properties.");
                                    DungeonGenerator.Init();
                                }
#endif
                                GenerateOptimalSolutionPath();
                                GenerateRoomTemplateForAllRooms();
                                GenerateObstacleBlockTemplateForAllRooms();
                                HandleReservedAndProbabilisticTilesForAllRooms();
                                GenerateDungeonBorder();

                                DungeonGenerator.isInit = false;

                                return dungeon;
                            }

                            private static void GenerateOptimalSolutionPath()
                            {
                                Coordinate currentCoordinate = new Coordinate(Roll(0, DungeonGenerator.dungeon.WidthInRooms - 1), DungeonGenerator.dungeon.HeightInRooms - 1);
                                Enumerables.Direction fromDirection = Enumerables.Direction.Up;
                                DungeonGenerator.dungeon.StartRoomCoordinate = currentCoordinate;

                                do
                                {
                                    Room currentRoom = DungeonGenerator.dungeon.Rooms[currentCoordinate.X, currentCoordinate.Y];
                                    currentRoom.Type = currentRoom.Type == RoomType.Side ? RoomType.LeftRight : currentRoom.Type;
                                    Enumerables.Direction nextDirection;

                                    do
                                    {
                                        nextDirection = GetNextDirection();
                                    } while (nextDirection == fromDirection);

                                    if ((nextDirection == Enumerables.Direction.Left && currentCoordinate.X == 0)
                                        || (nextDirection == Enumerables.Direction.Right && currentCoordinate.X == dungeon.WidthInRooms - 1)
                                        || (nextDirection == Enumerables.Direction.Down))
                                    {
                                        currentRoom.Type = currentRoom.Type == RoomType.LeftRightUp ? RoomType.LeftRightUpDown : RoomType.LeftRightDown;

                                        fromDirection = Enumerables.Direction.Up;
                                        currentCoordinate.Y--;

                                        if (currentCoordinate.Y >= 0)
                                        {
                                            currentRoom = DungeonGenerator.dungeon.Rooms[currentCoordinate.X, currentCoordinate.Y];
                                            currentRoom.Type = RoomType.LeftRightUp;
                                        }

                                        if (nextDirection == Enumerables.Direction.Left || nextDirection == Enumerables.Direction.Right)
                                        {
                                            fromDirection = nextDirection;
                                            currentCoordinate.X += nextDirection == Enumerables.Direction.Right ? -1 : 1;
                                        }
                                    }
                                    else
                                    {
                                        currentCoordinate.X += nextDirection == Enumerables.Direction.Right ? 1 : -1;
                                    }
                                } while (currentCoordinate.Y >= 0);

                                currentCoordinate.Y++;
                                dungeon.EndRoomCoordinate = currentCoordinate;
                            }

                            private static void GenerateRoomTemplateForAllRooms()
                            {
                                foreach (Room room in DungeonGenerator.dungeon)
                                {
                                    Template template;

                                    switch (room.Type)
                                    {
                                        case RoomType.LeftRight:
                                            template = DungeonGenerator.templates.leftRightRooms.GetRandom();
                                            break;

                                        case RoomType.LeftRightDown:
                                            template = DungeonGenerator.templates.leftRightDownRooms.GetRandom();
                                            break;

                                        case RoomType.LeftRightUp:
                                            template = DungeonGenerator.templates.leftRightUpRooms.GetRandom();
                                            break;

                                        case RoomType.LeftRightUpDown:
                                            template = DungeonGenerator.templates.leftRightUpDownRooms.GetRandom();
                                            break;

                                        default:
                                            template = DungeonGenerator.templates.sideRooms.GetRandom();
                                            break;
                                    }

                                    for (int x = 0; x < template.value[0].Length; x++)
                                    {
                                        for (int y = 0; y < template.value.Length; y++)
                                        {
                                            room.Tiles[x, y].Type = template.value[template.value.Length - y - 1][x];
                                        }
                                    }
                                }
 
                            }

                            private static void GenerateObstacleBlockTemplateForAllRooms()
                            {
                                foreach (Room room in DungeonGenerator.dungeon)
                                {
                                    foreach (Tile tile in room.Tiles)
                                    {
                                        if (tile.Type == TileType.AIR_OBSTACLE_BLOCK || tile.Type == TileType.LAND_OBSTACLE_BLOCK)
                                        {
                                            Template template = tile.Type == TileType.AIR_OBSTACLE_BLOCK 
                                                ? DungeonGenerator.templates.airObstacleBlocks.GetRandom() 
                                                : DungeonGenerator.templates.landObstacleBlocks.GetRandom();

                                            for (int x = tile.RoomCoordinate.X; x < tile.RoomCoordinate.X + template.value[0].Length; x++)
                                            {
                                                for (int y = tile.RoomCoordinate.Y; y < tile.RoomCoordinate.Y - template.value.Length; y--)
                                                {
                                                    char type = HandleProbabilisticTileType(template.value[template.value.Length - y - 1][x]);
                                                    room.Tiles[x, y].Type = type;
                                                }
                                            }

                                            for (int x = 0; x < template.value[0].Length; x++)
                                            {
                                                for (int y = 0; y < template.value.Length; y++)
                                                {
                                                    room.Tiles[tile.RoomCoordinate.X + x, tile.RoomCoordinate.Y - y].Type = template.value[y][x];
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            private static void HandleReservedAndProbabilisticTilesForAllRooms()
                            {
                                foreach (Room room in DungeonGenerator.dungeon)
                                {
                                    foreach (Tile tile in room.Tiles)
                                    {
                                        switch (tile.Type)
                                        {
                                            case TileType.RESERVED:
                                                room.ReservedTileCoordinate = tile.RoomCoordinate;
                                                tile.Type = TileType.EMPTY;
                                                break;

                                            case TileType.WALL_1_OF_2:
                                                tile.Type = Roll(0, 1) == 0 ? TileType.WALL : TileType.EMPTY;
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                }
                            }

                            private static void GenerateDungeonBorder()
                            {
                                int borderTilesCount = (DungeonGenerator.dungeon.WidthInTiles * 2) + (DungeonGenerator.dungeon.HeightInTiles * 2) + 4;
                                dungeon.BorderTiles = new Tile[borderTilesCount];

                                int i = 0;

                                for (int x = -1; x <= DungeonGenerator.dungeon.WidthInTiles; x++)
                                {
                                    dungeon.BorderTiles[i++] = new Tile(x, -1, TileType.WALL);
                                    dungeon.BorderTiles[i++] = new Tile(x, DungeonGenerator.dungeon.HeightInTiles, TileType.WALL);
                                }

                                for (int y = 0; y < DungeonGenerator.dungeon.HeightInTiles; y++)
                                {
                                    dungeon.BorderTiles[i++] = new Tile(-1, y, TileType.WALL);
                                    dungeon.BorderTiles[i++] = new Tile(DungeonGenerator.dungeon.WidthInTiles, y, TileType.WALL);
                                }
                            }

                            /// <summary>
                            /// Helper Functions
                            /// </summary>
                            /// <returns></returns>
                            private static Enumerables.Direction GetNextDirection()
                            {
                                int typeIndex = Roll(1, 5);

                                switch (typeIndex)
                                {
                                    case 1:
                                    case 2:
                                        return Enumerables.Direction.Left;
                                    case 3:
                                    case 4:
                                        return Enumerables.Direction.Right;
                                    default:
                                        return Enumerables.Direction.Down;
                                }
                            }

                            private static char HandleProbabilisticTileType(char value)
                            {
                                char result = TileType.EMPTY;

                                switch (value)
                                {
                                      case TileType.WALL_1_OF_2:
                                        break;

                                    default:
                                        result = value;
                                        break;
                                }

                                return result;
                            }

                            private static int Roll(int min, int max)
                            {
                                return UnityEngine.Random.Range(min, max + 1);
                            }
                        }
                    }
                }
            }
        }
    }
}
