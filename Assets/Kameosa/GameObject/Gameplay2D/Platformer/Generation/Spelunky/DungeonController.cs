﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kameosa.GameObject.Gameplay2D.Platformer.Generation.Spelunky;
using UnityEditor;
using Kameosa.Cartesian;
using Kameosa.Services;
using System.IO;
using System;

namespace Kameosa
{
    namespace GameObject
    {
        namespace Gameplay2D
        {
            namespace Platformer
            {
                namespace Generation
                {
                    namespace Spelunky
                    {
                        [RequireComponent(typeof(DungeonInstantiator))]
                        public class DungeonController : MonoBehaviour
                        {
                            private const float TILE_GIZMO_SIZE = 0.4f;

                            private DungeonInstantiator dungeonInstantiator;

                            /// <summary>
                            /// Tweaking variables
                            /// </summary>
                            [SerializeField]
                            private bool isShowGizmo = true;

                            [SerializeField]
                            private bool isShowRoomTypeGizmo = true;

                            [SerializeField]
                            private bool isShowTileTypeGizmo = true;

                            /// <summary>
                            /// State variables
                            /// </summary>
                            private Dungeon dungeon;

                            public event Action OnDungeonGenerated;

                            #region properties
                            public Coordinate StartCoordinate
                            {
                                get
                                {
                                    return this.dungeon.StartCoordinate;
                                }
                            }

                            public Dungeon Dungeon
                            {
                                get
                                {
                                    return this.dungeon;
                                }
                            }
                            #endregion

                            private void Awake()
                            {
                                this.dungeonInstantiator = GetComponent<DungeonInstantiator>();
                                Generate();
                            }

                            private void Start()
                            {
                            }

                            private void Update()
                            {
                            }

                            public void Generate()
                            {
                                this.dungeon = DungeonGenerator.Generate();
                                this.dungeonInstantiator.Instantiate(this.dungeon);

                                if (OnDungeonGenerated != null)
                                {
                                    OnDungeonGenerated();
                                }
                            }

                            void OnDrawGizmos()
                            {
                                if ((this.dungeon != null) && this.isShowGizmo)
                                {
                                    float tileSize = 1f;

                                    foreach (Room room in this.dungeon)
                                    {
                                        foreach (Tile tile in room)
                                        {
                                            UnityEngine.Vector3 position = new UnityEngine.Vector3(tile.DungeonCoordinate.X * tileSize, tile.DungeonCoordinate.Y * tileSize, 0);

                                            UnityEngine.Gizmos.color = Color.white;

                                            if (this.dungeon.StartRoomCoordinate.Equals(room.Coordinate))
                                            {
                                                UnityEngine.Gizmos.color = Color.green;
                                            }
                                            else if (this.dungeon.EndRoomCoordinate.Equals(room.Coordinate))
                                            {
                                                UnityEngine.Gizmos.color = Color.red;
                                            }
                                            else if (room.Type == RoomType.Side)
                                            {
                                                UnityEngine.Gizmos.color = Color.black;
                                            }

                                            if (this.isShowRoomTypeGizmo)
                                            {
                                                Handles.Label(position, ((int)room.Type).ToString());
                                            }
                                            else if (this.isShowTileTypeGizmo)
                                            {
                                                Handles.Label(position, ((char)tile.Type).ToString());
                                            }
                                            else
                                            {
                                                UnityEngine.Gizmos.DrawCube(position, UnityEngine.Vector3.one * TILE_GIZMO_SIZE);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
