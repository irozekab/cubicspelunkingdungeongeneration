﻿namespace Kameosa
{
    namespace GameObject
    {
        namespace Gameplay2D
        {
            namespace Platformer
            {
                namespace Generation
                {
                    namespace Spelunky
                    {
                        [System.Serializable]
                        public class Template
                        {
                            public string[] value;
                        }
                    }
                }
            }
        }
    }
}