﻿namespace Kameosa
{
    namespace GameObject
    {
        namespace Gameplay2D
        {
            namespace Platformer
            {
                namespace Generation
                {
                    namespace Spelunky
                    {
                        [System.Serializable]
                        public class Templates
                        {
                            public Template[] sideRooms;
                            public Template[] leftRightRooms;
                            public Template[] leftRightDownRooms;
                            public Template[] leftRightUpRooms;
                            public Template[] leftRightUpDownRooms;

                            public Template[] airObstacleBlocks;
                            public Template[] landObstacleBlocks;
                        }
                    }
                }
            }
        }
    }
}