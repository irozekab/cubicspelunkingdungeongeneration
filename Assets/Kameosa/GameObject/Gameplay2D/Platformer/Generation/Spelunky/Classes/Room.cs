﻿using System.Collections;
using Kameosa.Cartesian;

namespace Kameosa
{
    namespace GameObject
    {
        namespace Gameplay2D
        {
            namespace Platformer
            {
                namespace Generation
                {
                    namespace Spelunky
                    {
                        public class Room : IEnumerable
                        {
                            private Dungeon dungeon;
                            private Coordinate coordinate;
                            private Coordinate reservedTileCoordinate;
                            private Tile[,] tiles;
                            private RoomType type;

                            #region properties

                            public RoomType Type
                            {
                                get
                                {
                                    return this.type;
                                }

                                set
                                {
                                    this.type = value;
                                }
                            }

                            public Coordinate Coordinate
                            {
                                get
                                {
                                    return this.coordinate;
                                }

                                set
                                {
                                    this.coordinate = new Coordinate(value.X, value.Y);
                                }
                            }

                            public int Width 
                            {
                                get
                                {
                                    return this.tiles.GetLength(0);
                                }
                            }

                            public int Height 
                            {
                                get
                                {
                                    return this.tiles.GetLength(1);
                                }
                            }

                            public Tile[,] Tiles
                            {
                                get
                                {
                                    return this.tiles;
                                }
                            }

                            public Coordinate ReservedTileCoordinate
                            {
                                get
                                {
                                    return this.reservedTileCoordinate;
                                }

                                set
                                {
                                    this.reservedTileCoordinate = value;
                                }
                            }

                            public Tile ReservedTile
                            {
                                get
                                {
                                    return this.tiles[this.reservedTileCoordinate.X, this.reservedTileCoordinate.Y];
                                }
                            }

                            #endregion

                            public Room(Dungeon dungeon, int x, int y, int roomHeightInTiles = 8, int roomWidthInTiles = 10, RoomType type = RoomType.Side)
                            {
                                this.dungeon = dungeon;
                                this.coordinate = new Coordinate(x, y);
                                this.type = type;

                                this.tiles = new Tile[roomWidthInTiles, roomHeightInTiles];

                                for (int _x = 0; _x < roomWidthInTiles; _x++)
                                {
                                    for (int _y = 0; _y < roomHeightInTiles; _y++)
                                    {
                                        this.tiles[_x, _y] = new Tile(this, _x, _y);
                                    }
                                }
                            }

                            public IEnumerator GetEnumerator()
                            {
                                for (int x = 0; x < this.tiles.GetLength(0); x++)
                                {
                                    for (int y = 0; y < this.tiles.GetLength(1); y++)
                                    {
                                        yield return this.tiles[x, y];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
