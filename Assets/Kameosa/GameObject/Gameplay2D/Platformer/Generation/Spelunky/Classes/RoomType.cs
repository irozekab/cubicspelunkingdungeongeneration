﻿namespace Kameosa
{
    namespace GameObject
    {
        namespace Gameplay2D
        {
            namespace Platformer
            {
                namespace Generation
                {
                    namespace Spelunky
                    {
                        public enum RoomType
                        {
                            Side,
                            LeftRight,
                            LeftRightDown,
                            LeftRightUp,
                            LeftRightUpDown,
                        }
                    }
                }
            }
        }
    }
}
