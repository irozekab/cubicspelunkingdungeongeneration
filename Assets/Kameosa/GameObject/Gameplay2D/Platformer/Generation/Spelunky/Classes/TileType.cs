﻿namespace Kameosa
{
    namespace GameObject
    {
        namespace Gameplay2D
        {
            namespace Platformer
            {
                namespace Generation
                {
                    namespace Spelunky
                    {
                        public static class TileType 
                        {
                            public const char EMPTY = '0';
                            public const char WALL = '1';
                            public const char RESERVED = 'R';
                            public const char LADDER = 'L';
                            public const char LADDER_TOP = 'P';
                            public const char WALL_1_OF_2 = '2';
                            public const char PUSH_BLOCK_1_OF_4 = '4';
                            public const char SPIKE_1_OF_3 = '7';
                            public const char MOVING_PLATFORM = '_';
                            public const char LAND_OBSTACLE_BLOCK = '5';
                            public const char AIR_OBSTACLE_BLOCK = '6';
                        }
                    }
                }
            }
        }
    }
}