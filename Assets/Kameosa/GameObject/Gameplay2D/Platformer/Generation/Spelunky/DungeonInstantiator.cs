﻿using System.Collections.Generic;
using Kameosa.Collections.IList;
using UnityEngine;
using Kameosa.GameObject;

namespace Kameosa
{
    namespace GameObject
    {
        namespace Gameplay2D
        {
            namespace Platformer
            {
                namespace Generation
                {
                    namespace Spelunky
                    {
                        public class DungeonInstantiator : MonoBehaviour
                        {
                            [SerializeField]
                            List<UnityEngine.GameObject> spaces;

                            [SerializeField]
                            List<UnityEngine.GameObject> walls;

                            [SerializeField]
                            UnityEngine.GameObject movingPlatorm;

                            [SerializeField]
                            UnityEngine.GameObject exit;

                            public void Instantiate(Dungeon dungeon)
                            {
                                gameObject.DestroyChildren();

                                foreach (Room room in dungeon)
                                {
                                    UnityEngine.GameObject roomGameObject = new UnityEngine.GameObject(room.Coordinate.ToString());
                                    roomGameObject.transform.SetParent(this.transform);

                                    foreach (Tile tile in room)
                                    {
                                        UnityEngine.GameObject tilePrefab;

                                        switch (tile.Type)
                                        {
                                            case TileType.WALL:
                                                tilePrefab = this.walls.GetRandom();
                                                break;

                                            case TileType.MOVING_PLATFORM:
                                                tilePrefab = this.movingPlatorm;
                                                break;

                                            default:
                                                continue;
                                                tilePrefab = this.spaces.GetRandom();
                                                break;
                                        }

                                        UnityEngine.GameObject.Instantiate(tilePrefab, tile.DungeonCoordinate, Quaternion.identity, roomGameObject.transform);
                                    }
                                }

                                UnityEngine.GameObject borderGameObject = new UnityEngine.GameObject("Dungeon Border");
                                borderGameObject.transform.SetParent(this.transform);

                                foreach (Tile tile in dungeon.BorderTiles)
                                {
                                    UnityEngine.GameObject tilePrefab = this.walls.GetRandom();
                                    UnityEngine.GameObject.Instantiate(tilePrefab, tile.DungeonCoordinate, Quaternion.identity, borderGameObject.transform);
                                }

                                UnityEngine.GameObject.Instantiate(this.exit, dungeon.EndCoordinate, Quaternion.identity, borderGameObject.transform);
                            }
                        }
                    }
                }
            }
        }
    }
}