﻿using UnityEngine;
using UnityEngine.UI;

namespace Kameosa
{
    namespace GameObject
    {
        namespace Common
        {
            namespace Controller
            {
                public abstract class BaseHudController : MonoBehaviour
                {
                    [SerializeField]
                    protected BaseGameplayController baseGameplayController;

                    [SerializeField]
                    protected Button pauseButton;

                    [SerializeField]
                    protected Button resumeButton;

                    [SerializeField]
                    protected Button quitButton;

                    [SerializeField]
                    protected UnityEngine.GameObject pausePanel;

                    [SerializeField]
                    protected UnityEngine.GameObject gameEndPanel;

                    protected virtual void Awake()
                    {
                        if (this.baseGameplayController == null)
                        {
                            this.baseGameplayController = UnityEngine.GameObject.Find("Gameplay").GetComponent<BaseGameplayController>();
                        }

                        if (this.pauseButton == null)
                        {
                            this.pauseButton = UnityEngine.GameObject.Find("PauseButton").GetComponent<Button>();
                            this.pauseButton.onClick.AddListener(GamePause);
                        }

                        if (this.resumeButton == null)
                        {
                            this.resumeButton = UnityEngine.GameObject.Find("ResumeButton").GetComponent<Button>();
                            this.resumeButton.onClick.AddListener(GameResume);
                        }

                        if (this.quitButton == null)
                        {
                            this.quitButton = UnityEngine.GameObject.Find("QuitButton").GetComponent<Button>();
                            this.quitButton.onClick.AddListener(GameQuit);
                        }

                        if (this.pausePanel == null)
                        {
                            this.pausePanel = UnityEngine.GameObject.Find("PausePanel");
                        }

                        if (this.gameEndPanel == null)
                        {
                            this.gameEndPanel = UnityEngine.GameObject.Find("GameEndPanel");
                        }
                    }

                    protected virtual void OnEnable()
                    {
                        this.baseGameplayController.OnGameAwake += OnGameAwake;
                        //this.baseGameplayController.OnGameStart += OnGameStart;
                        //this.baseGameplayController.OnGamePause += OnGamePause;
                        //this.baseGameplayController.OnGameResume += OnGameResume;
                        this.baseGameplayController.OnGameWon += OnGameWon;
                        this.baseGameplayController.OnGameLose += OnGameLose;
                        this.baseGameplayController.OnGameEnd += OnGameEnd;
                        //this.baseGameplayController.OnGameRestart += OnGameRestart;
                        //this.baseGameplayController.OnGameQuit += OnGameQuit;
                    }

                    protected virtual void Start()
                    {
                        SetPausePanelActive(false);
                        SetGameEndPanelActive(false);
                    }

                    protected virtual void OnDisable()
                    {
                        this.baseGameplayController.OnGameAwake -= OnGameAwake;
                        //this.baseGameplayController.OnGameStart -= OnGameStart;
                        //this.baseGameplayController.OnGamePause -= OnGamePause;
                        //this.baseGameplayController.OnGameResume -= OnGameResume;
                        this.baseGameplayController.OnGameWon -= OnGameWon;
                        this.baseGameplayController.OnGameLose -= OnGameLose;
                        this.baseGameplayController.OnGameEnd -= OnGameEnd;
                        //this.baseGameplayController.OnGameRestart -= OnGameRestart;
                        //this.baseGameplayController.OnGameQuit -= OnGameQuit;
                    }

                    public virtual void GameStart()
                    {
                        this.baseGameplayController.GameStart();
                    }

                    public virtual void GamePause()
                    {
                        this.baseGameplayController.GamePause();

                        SetPauseButtonActive(false);
                        SetPausePanelActive(true);
                    }

                    public virtual void GameResume()
                    {
                        this.baseGameplayController.GameResume();

                        SetPauseButtonActive(true);
                        SetPausePanelActive(false);
                    }

                    public virtual void GameRestart()
                    {
                        this.baseGameplayController.GameRestart();
                    }

                    public virtual void GameQuit()
                    {
                        this.baseGameplayController.GameQuit();
                    }

                    protected virtual void OnGameAwake()
                    {
                    }

                    //protected virtual void OnGameStart()
                    //{
                    //}

                    //protected virtual void OnGamePause()
                    //{
                    //}

                    //protected virtual void OnGameResume()
                    //{
                    //}


                    protected virtual void OnGameWon()
                    {
                    }


                    protected virtual void OnGameLose()
                    {
                    }


                    protected virtual void OnGameEnd()
                    {
                        SetPauseButtonActive(false);
                        SetGameEndPanelActive(true);
                    }

                    //protected virtual void OnGameRestart()
                    //{
                    //}

                    //protected virtual void OnGameQuit()
                    //{
                    //}

                    protected virtual void SetPauseButtonActive(bool isActive)
                    {
                        if (this.pauseButton)
                        {
                            this.pauseButton.gameObject.SetActive(isActive);
                        }
                    }

                    protected virtual void SetPausePanelActive(bool isActive)
                    {
                        if (this.pausePanel)
                        {
                            this.pausePanel.SetActive(isActive);
                        }
                    }

                    protected virtual void SetGameEndPanelActive(bool isActive)
                    {
                        if (this.gameEndPanel)
                        {
                            this.gameEndPanel.SetActive(isActive);
                        }
                    }
                }
            }
        }
    }
}