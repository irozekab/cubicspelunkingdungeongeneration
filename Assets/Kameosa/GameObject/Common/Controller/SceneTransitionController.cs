﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Kameosa
{
    namespace GameObject
    {
        namespace Common
        {
            namespace Controller
            {
                public class SceneTransitionController : MonoBehaviour
                {
                    protected const float DEFAULT_TRANSITION_DURATION = 1f;

                    protected UnityEngine.GameObject transitionPanel;
                    protected Animator animator;
                    protected float animationClipDuration = 1f;
                    protected float transitionDuration = DEFAULT_TRANSITION_DURATION;

                    #region Properties

                    public float TransitionDuration
                    {
                        get
                        {
                            return this.transitionDuration;
                        }

                        set
                        {
                            this.transitionDuration = value;
                        }
                    }

                    #endregion

                    #region Singleton Pattern

                    private static SceneTransitionController instance;

                    public static SceneTransitionController Instance
                    {
                        get
                        {
                            return SceneTransitionController.instance;
                        }
                    }

                    private void Awake()
                    {
                        if (SceneTransitionController.instance != null)
                        {
                            Destroy(this.gameObject);
                        }
                        else
                        {
                            SceneTransitionController.instance = this;
                            DontDestroyOnLoad(this.gameObject);
                        }

                        if (this.transitionPanel == null)
                        {
                            this.transitionPanel = this.transform.Find("TransitionPanel").gameObject;
                        }

                        if (this.animator == null)
                        {
                            this.animator = this.transitionPanel.GetComponent<Animator>();
                        }
                    }

                    #endregion

                    protected virtual void Start()
                    {
                        Initialize();

                        AnimationClip[] animationClips = this.animator.runtimeAnimatorController.animationClips;

                        foreach (AnimationClip animationClip in animationClips)
                        {
                            if (animationClip.name == "FadeOut")
                            {
                                this.animationClipDuration = animationClip.length;
                                break;
                            }
                        }
                    }

                    protected void Initialize()
                    {
                    }

                    public void LoadScene(string scene)
                    {
                        StartCoroutine(FadeIn(scene));
                    }

                    public void ReloadScene()
                    {
                        StartCoroutine(FadeIn(SceneManager.GetActiveScene().name));
                    }

                    protected virtual IEnumerator FadeIn(string scene)
                    {
                        this.transitionPanel.SetActive(true);
                        this.animator.Play("FadeIn");

                        yield return new WaitForSecondsRealtime(this.transitionDuration);

                        SceneManager.LoadScene(scene);

                        StartCoroutine(FadeOut());
                    }

                    protected virtual IEnumerator FadeOut()
                    {
                        this.animator.Play("FadeOut");

                        yield return new WaitForSecondsRealtime(this.animationClipDuration);

                        this.transitionPanel.SetActive(false);
                    }
                }
            }
        }
    }
}