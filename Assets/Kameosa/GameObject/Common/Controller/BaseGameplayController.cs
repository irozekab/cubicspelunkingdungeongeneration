﻿using System;
using System.Collections;
using Kameosa.Common;
using UnityEngine;

namespace Kameosa
{
    namespace GameObject
    {
        namespace Common
        {
            namespace Controller
            {
                public abstract class BaseGameplayController : MonoBehaviour
                {
                    [SerializeField]
                    protected System.Tuple<float, float> xBounds = new System.Tuple<float, float>(Int32.MinValue, Int32.MaxValue);

                    [SerializeField]
                    protected System.Tuple<float, float> yBounds = new System.Tuple<float, float>(Int32.MinValue, Int32.MaxValue);

                    public event Action OnGameAwake;
                    public event Action OnGameStart;
                    public event Action OnGamePause;
                    public event Action OnGameResume;
                    public event Action OnGameWon;
                    public event Action OnGameLose;
                    public event Action OnGameEnd;
                    public event Action OnGameRestart;
                    public event Action OnGameQuit;

                    #region Properties

                    public System.Tuple<float, float> XBounds
                    {
                        get
                        {
                            return this.xBounds;
                        }

                        set
                        {
                            this.xBounds = value;
                        }
                    }

                    public System.Tuple<float, float> YBounds
                    {
                        get
                        {
                            return this.yBounds;
                        }

                        set
                        {
                            this.yBounds = value;
                        }
                    }

                    #endregion

                    protected virtual void GameAwake()
                    {
#if UNITY_EDITOR
                        Debug.Log("GameAwake()");
#endif

                        if (OnGameAwake != null)
                        {
                            OnGameAwake();
                        }
                    }

                    public virtual void GameStart()
                    {
                        GameAwake();

#if UNITY_EDITOR
                        Debug.Log("GameStart()");
#endif

                        if (OnGameStart != null)
                        {
                            OnGameStart();
                        }
                    }

                    public virtual void GamePause()
                    {
#if UNITY_EDITOR
                        Debug.Log("GamePause()");
#endif

                        if (OnGamePause != null)
                        {
                            OnGamePause();
                        }
                    }

                    public virtual void GameResume()
                    {
#if UNITY_EDITOR
                        Debug.Log("GameResume()");
#endif

                        if (OnGameResume != null)
                        {
                            OnGameResume();
                        }
                    }

                    protected virtual void GameWon()
                    {
#if UNITY_EDITOR
                        Debug.Log("GameWon()");
#endif

                        if (OnGameWon != null)
                        {
                            OnGameWon();
                        }

                        GameEnd();
                    }

                    protected virtual void GameLose()
                    {
#if UNITY_EDITOR
                        Debug.Log("GameLose()");
#endif

                        if (OnGameLose != null)
                        {
                            OnGameLose();
                        }

                        GameEnd();
                    }

                    protected virtual void GameEnd()
                    {
#if UNITY_EDITOR
                        Debug.Log("GameEnd()");
#endif

                        if (OnGameEnd != null)
                        {
                            OnGameEnd();
                        }
                    }

                    public virtual void GameRestart()
                    {
#if UNITY_EDITOR
                        Debug.Log("GameRestart()");
#endif

                        if (OnGameRestart != null)
                        {
                            OnGameRestart();
                        }
                    }

                    public virtual void GameQuit()
                    {
#if UNITY_EDITOR
                        Debug.Log("GameQuit()");
#endif

                        if (OnGameQuit != null)
                        {
                            OnGameQuit();
                        }
                    }

                    protected IEnumerator UnfreezeTimeScale()
                    {
                        yield return new WaitForSecondsRealtime(1f);
                        Time.timeScale = 1f;
                    }
                }
            }
        }
    }
}