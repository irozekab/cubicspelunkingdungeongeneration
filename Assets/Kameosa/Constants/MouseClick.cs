﻿namespace Kameosa
{
    namespace Constants
    {
        public static class MouseClick 
        {
            public const int LEFT = 0;
            public const int RIGHT = 1;
        }
    }
}
