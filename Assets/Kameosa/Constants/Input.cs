﻿namespace Kameosa
{
    namespace Constants
    {
        public static class Input
        {
            public const string HORIZONTAL = "Horizontal";
            public const string VERTICAL = "Vertical";
            public const string JUMP = "Jump";
            public const string DASH = "Fire1";
        }
    }
}
