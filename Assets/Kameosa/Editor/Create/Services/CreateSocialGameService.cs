﻿#if KAMEOSA_SOCIAL
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SocialPlatforms;

namespace Kameosa
{
    namespace Editor
    {
        namespace Create
        {
            namespace Services
            {    
                public static class CreateSocialGameService
                {
                    private const string CREATE_SOCIAL_GAME_SERVICE = "CREATE_SOCIAL_GAME_SERVICE";
                    private const string SOCIAL_GAME_SERVICE = "SocialGameService";
                    private const string SOCIAL_GAME_SERVICE_SCRIPT_TEMPLATE_PATH = Constants.KAMEOSA_EDITOR_SCRIPT_TEMPLATES_FOLDER_PATH + "\\SocialGameService.txt";
                    private const string SOCIAL_GAME_SERVICE_SCRIPT_PATH = Constants.SCRIPTS_SERVICES_FOLDER_PATH + "\\SocialGameService.cs";

                    [MenuItem("Kameosa/Create/Services/Social Game Service")]
                    public static void Main()
                    {
                        Common.CopyAsset(SOCIAL_GAME_SERVICE_SCRIPT_TEMPLATE_PATH, SOCIAL_GAME_SERVICE_SCRIPT_PATH);

                        EditorPrefs.SetBool(CREATE_SOCIAL_GAME_SERVICE, true);
                    }

                    [UnityEditor.Callbacks.DidReloadScripts]
                    private static void OnDidReloadScripts()
                    {
                        if (EditorPrefs.HasKey(CREATE_SOCIAL_GAME_SERVICE))
                        {
                            EditorPrefs.DeleteKey(CREATE_SOCIAL_GAME_SERVICE);

                            Kameosa.Services.LogService.Info("CreateSocialGameService: OnDidReloadScripts()");

                            if (GameObject.Find(SOCIAL_GAME_SERVICE) != null)
                            {
                                UnityEngine.Object.DestroyImmediate(GameObject.Find(SOCIAL_GAME_SERVICE));
                            }

                            GameObject socialGameService = new GameObject(SOCIAL_GAME_SERVICE);

                            MonoScript socialGameServiceScript = AssetDatabase.LoadMainAssetAtPath(SOCIAL_GAME_SERVICE_SCRIPT_PATH) as MonoScript;
                            socialGameService.AddComponent(socialGameServiceScript.GetClass());
                        }
                    }
                }
            }
        }
    }
}
#endif