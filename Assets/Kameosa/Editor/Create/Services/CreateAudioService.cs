﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Kameosa
{
    namespace Editor
    {
        namespace Create
        {
            namespace Services
            {
                public static class CreateAudioService
                {
                    private const string NAME = "AudioService";
                    private const string GAMEOBJECT_NAME = NAME;
                    private const string FILE_NAME = NAME;
                    private const string MENU_ITEM = "Kameosa/Create/GameObjects/" + FILE_NAME;

                    [MenuItem(MENU_ITEM)]
                    public static void Main()
                    {
                        Common.CreateScriptFromTemplate(FILE_NAME, Constants.SCRIPTS_SERVICES_FOLDER_PATH);

                        EditorPrefs.SetBool(NAME, true);
                    }

                    [UnityEditor.Callbacks.DidReloadScripts]
                    private static void OnDidReloadScripts()
                    {
                        if (EditorPrefs.HasKey(NAME))
                        {
                            EditorPrefs.DeleteKey(NAME);

                            Kameosa.Services.LogService.Info("Create" + NAME + ": OnDidReloadScripts()");

                            if (UnityEngine.GameObject.Find(GAMEOBJECT_NAME) != null)
                            {
                                UnityEngine.Object.DestroyImmediate(UnityEngine.GameObject.Find(GAMEOBJECT_NAME));
                            }

                            UnityEngine.GameObject gameObject = Common.CreateGameObject(NAME);
                            gameObject.AddComponent<AudioSource>();
                            Common.AttachScriptToGameObject(Common.GetServiceScriptPath(FILE_NAME), gameObject);
                            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
                        }
                    }
                }
            }
        }
    }
}