﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Kameosa
{
    namespace Editor
    {
        namespace Create
        {
            namespace Services
            {
                public static class CreateGameManager
                {
                    private const string NAME = "GameManager";
                    private const string GAMEOBJECT_NAME = NAME;
                    private const string FILE_NAME = NAME;
                    private const string MENU_ITEM = "Kameosa/Create/Scene/" + FILE_NAME;

                    [MenuItem(MENU_ITEM)]
                    public static void Main()
                    {
                        Common.CreateScriptFromTemplate(FILE_NAME, Constants.SCRIPTS_SERVICES_FOLDER_PATH);
                        Create.Scripts.CreateGameData.Main();

                        EditorPrefs.SetBool(NAME, true);
                    }

                    [UnityEditor.Callbacks.DidReloadScripts]
                    private static void OnDidReloadScripts()
                    {
                        if (EditorPrefs.HasKey(NAME))
                        {
                            EditorPrefs.DeleteKey(NAME);

                            Kameosa.Services.LogService.Info("Create" + NAME + ": OnDidReloadScripts()");

                            if (UnityEngine.GameObject.Find(NAME) != null)
                            {
                                UnityEngine.Object.DestroyImmediate(UnityEngine.GameObject.Find(NAME));
                            }

                            UnityEngine.GameObject gameObject = Common.CreateGameObject(NAME);
                            Common.AttachScriptToGameObject(Common.GetServiceScriptPath(FILE_NAME), gameObject);
                            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
                        }
                    }
                }
            }
        }
    }
}