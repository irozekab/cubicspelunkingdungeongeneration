﻿using System.IO;
using UnityEditor;
using UnityEditor.Animations;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

namespace Kameosa
{
    namespace Editor
    {
        namespace Create
        {
            namespace Services
            {
                public static class CreateSceneTransitionService
                {
                    private const string NAME = "SceneTransitionService";
                    private const string GAMEOBJECT_NAME = NAME;
                    private const string FILE_NAME = NAME;
                    private const string MENU_ITEM = "Kameosa/Create/GameObjects/" + FILE_NAME;

                    private const string SCENE_TRANSITION_ANIMATION_FOLDER_PATH = Constants.ANIMATIONS_FOLDER_PATH + "\\" + NAME;
                    private const string FADE_ANIMATOR_CONTROLLER_PATH = SCENE_TRANSITION_ANIMATION_FOLDER_PATH + "\\Fade.controller";
                    private const string FADE_IN_ANIMATION_PATH = SCENE_TRANSITION_ANIMATION_FOLDER_PATH + "\\FadeIn.anim";

                    private const float DEFAULT_TRANSITION_DURATION = 1f;

                    [MenuItem(MENU_ITEM)]
                    public static void Main()
                    {
                        CreateSceneTransitionAnimation();
                        CreateSceneTransitionAnimatorController();
                        Common.CreateScriptFromTemplate(FILE_NAME, Constants.SCRIPTS_SERVICES_FOLDER_PATH);

                        EditorPrefs.SetBool(NAME, true);
                    }

                    private static void CreateSceneTransitionAnimation()
                    {
                        Editor.Common.CreateFolder(SCENE_TRANSITION_ANIMATION_FOLDER_PATH);

                        AnimationClip animationClip = new AnimationClip { name = "FadeIn" };
                        animationClip.SetCurve("", typeof(Image), "m_Color.a", AnimationCurve.Linear(0f, 0f, DEFAULT_TRANSITION_DURATION, 1f));

                        AssetDatabase.CreateAsset(animationClip, FADE_IN_ANIMATION_PATH);
                    }

                    private static void CreateSceneTransitionAnimatorController()
                    {
                        AnimatorController animatorController = AnimatorController.CreateAnimatorControllerAtPath(FADE_ANIMATOR_CONTROLLER_PATH);

                        AnimatorStateMachine stateMachine = animatorController.layers[0].stateMachine;
                        AnimatorState fadeInState = stateMachine.AddState("FadeIn");
                        AnimatorState fadeOutState = stateMachine.AddState("FadeOut");
                        AnimatorState idleState = stateMachine.AddState("Idle");
                        stateMachine.defaultState = idleState;

                        AnimationClip animationClip = AssetDatabase.LoadAssetAtPath<AnimationClip>(FADE_IN_ANIMATION_PATH);
                        fadeInState.motion = animationClip;
                        fadeOutState.motion = animationClip;
                        fadeOutState.speed = -1f;
                    }

                    [UnityEditor.Callbacks.DidReloadScripts]
                    private static void OnDidReloadScripts()
                    {
                        if (EditorPrefs.HasKey(NAME))
                        {
                            EditorPrefs.DeleteKey(NAME);

                            Kameosa.Services.LogService.Info("Create" + NAME + ": OnDidReloadScripts()");

                            if (UnityEngine.GameObject.Find(GAMEOBJECT_NAME) != null)
                            {
                                UnityEngine.Object.DestroyImmediate(UnityEngine.GameObject.Find(GAMEOBJECT_NAME));
                            }

                            UnityEngine.GameObject sceneTransitionService = Common.CreateCanvas(GAMEOBJECT_NAME);
                            sceneTransitionService.GetComponent<Canvas>().sortingOrder = Kameosa.Math.Constants.MAX_INT_16BIT_SIGNED;

                            Common.AttachScriptToGameObject(Common.GetServiceScriptPath(NAME), sceneTransitionService);

                            UnityEngine.GameObject transitionPanel = Common.CreatePanel("TransitionPanel", sceneTransitionService);
                            transitionPanel.SetActive(false);

                            Animator animator = transitionPanel.AddComponent<Animator>();
                            AnimatorController animatorController = AssetDatabase.LoadAssetAtPath<AnimatorController>(FADE_ANIMATOR_CONTROLLER_PATH);
                            animator.runtimeAnimatorController = animatorController;
                            animator.updateMode = AnimatorUpdateMode.UnscaledTime;

                            Color color = Color.black;
                            color.a = 0f;
                            transitionPanel.GetComponent<Image>().color = color;

                            RectTransform rectTransform = transitionPanel.GetComponent<RectTransform>();
                            rectTransform.anchorMin = UnityEngine.Vector2.zero;
                            rectTransform.anchorMax = UnityEngine.Vector2.one;
                            rectTransform.sizeDelta = UnityEngine.Vector2.zero;

                            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
                        }
                    }
                }
            }
        }
    }
}