﻿using UnityEditor;
using UnityEngine;

namespace Kameosa
{
    namespace Editor
    {
        namespace Create
        {
			namespace GameObjects 
			{	
	            public static class CreateOnTriggerDestroyer
	            {
                    private const string NAME = "OnTriggerDestroyer";
                    private const string CONTROLLER_NAME = NAME;
                    private const string MENU_ITEM = "Kameosa/Create/GameObjects/" + CONTROLLER_NAME;
                    private const string SCRIPT_PATH = Constants.SCRIPTS_GAMEPLAY_SPAWNERS_DESTROYERS_FOLDER_PATH + "\\" + CONTROLLER_NAME + ".cs";

	                private const string PARENT_NAME = "SpawnersDestroyers";
                    private const float DEFAULT_DESTROYER_X = -20f;
                    private const float DEFAULT_DESTROYER_Y = 0f;
                    private const float DEFAULT_DESTROYER_WIDTH = 1f;
                    private const float DEFAULT_DESTROYER_HEIGHT = 20f;

                    [MenuItem(MENU_ITEM)]
	                public static void Main()
	                {
                        Kameosa.Editor.Common.CopyAsset(Common.GetScriptTemplatePath(CONTROLLER_NAME), SCRIPT_PATH, true);

                        UnityEngine.GameObject camera = Camera.main.gameObject;
                        Common.CreateGameObject(PARENT_NAME, camera);

                        EditorPrefs.SetBool(NAME, true);
	                }

	                [UnityEditor.Callbacks.DidReloadScripts]
	                private static void OnDidReloadScripts()
	                {
	                    if (EditorPrefs.HasKey(NAME))
	                    {
	                        EditorPrefs.DeleteKey(NAME);

                            Kameosa.Services.LogService.Info("Create" + CONTROLLER_NAME + ": OnDidReloadScripts()");

                            UnityEngine.GameObject parent = UnityEngine.GameObject.Find(PARENT_NAME);
                            UnityEngine.GameObject gameObject = Common.CreateGameObject(CONTROLLER_NAME, parent);
                            gameObject.AddComponent<BoxCollider2D>();
                            gameObject.GetComponent<BoxCollider2D>().size = new UnityEngine.Vector2(DEFAULT_DESTROYER_WIDTH, DEFAULT_DESTROYER_HEIGHT);
                            gameObject.GetComponent<BoxCollider2D>().isTrigger = true;
                            gameObject.AddComponent<Rigidbody2D>();
                            gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
                            gameObject.transform.position = new UnityEngine.Vector2(DEFAULT_DESTROYER_X, DEFAULT_DESTROYER_Y);

	                        if (gameObject != null)
	                        {
                                Common.AttachScriptToGameObject(SCRIPT_PATH, gameObject);
	                        }
	                    }
	                }
	            }
	        }
	    }
	}
}