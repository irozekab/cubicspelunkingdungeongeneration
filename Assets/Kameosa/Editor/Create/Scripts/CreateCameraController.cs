﻿using UnityEditor;
using UnityEngine;

namespace Kameosa
{
    namespace Editor
    {
        namespace Create
        {
            namespace Scripts 
            {    
                public static class CreateMainCameraController 
                {
                    private const string NAME = "MainCamera";
                    private const string GAMEOBJECT_NAME = NAME;
                    private const string FILE_NAME = NAME + "Controller";
                    private const string MENU_ITEM = "Kameosa/Create/Scripts/" + FILE_NAME;

                    [MenuItem(MENU_ITEM)]
                    public static void Main()
                    {
                        Common.CreateScriptFromTemplate(FILE_NAME, Constants.SCRIPTS_GAMEPLAY_FOLDER_PATH);

                        EditorPrefs.SetBool(NAME, true);
                    }

                    [UnityEditor.Callbacks.DidReloadScripts]
                    private static void OnDidReloadScripts()
                    {
                        if (EditorPrefs.HasKey(NAME))
                        {
                            EditorPrefs.DeleteKey(NAME);

                            Kameosa.Services.LogService.Info("Create" + FILE_NAME + ": OnDidReloadScripts()");

                            UnityEngine.GameObject gameObject = Camera.main.gameObject;

                            if (gameObject != null)
                            {
                                Common.AttachScriptToGameObject(Common.GetScriptPath(Constants.SCRIPTS_GAMEPLAY_FOLDER_PATH, FILE_NAME), gameObject);
                            }
                        }
                    }
                }
            }
        }
    }
}