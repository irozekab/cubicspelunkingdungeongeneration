﻿using UnityEditor;
using UnityEngine;

namespace Kameosa
{
    namespace Editor
    {
        namespace Create
        {
            namespace Scripts 
            {    
                public static class CreateHudController 
                {
                    private const string NAME = "Hud";
                    private const string GAMEOBJECT_NAME = NAME;
                    private const string FILE_NAME = NAME + "Controller";
                    private const string MENU_ITEM = "Kameosa/Create/Scripts/" + FILE_NAME;

                    [MenuItem(MENU_ITEM)]
                    public static void Main()
                    {
                        Common.CreateScriptFromTemplate(FILE_NAME, Constants.SCRIPTS_GAMEPLAY_FOLDER_PATH);
                        EditorPrefs.SetBool(NAME, true);
                    }

                    [UnityEditor.Callbacks.DidReloadScripts]
                    private static void OnDidReloadScripts()
                    {
                        if (EditorPrefs.HasKey(NAME))
                        {
                            EditorPrefs.DeleteKey(NAME);

                            if (Preferences.isPrintLog)
                            {
                                Debug.Log("Create" + FILE_NAME + ": OnDidReloadScripts()");
                            }

                            UnityEngine.GameObject gameObject = UnityEngine.GameObject.FindObjectOfType<Canvas>().gameObject;

                            if (gameObject != null)
                            {
                                Common.AttachScriptToGameObject(Common.GetScriptPath(FILE_NAME, Constants.SCRIPTS_GAMEPLAY_FOLDER_PATH), gameObject);
                            }
                        }
                    }
                }
            }
        }
    }
}