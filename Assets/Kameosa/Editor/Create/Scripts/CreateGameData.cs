﻿using UnityEditor;
using UnityEngine;

namespace Kameosa
{
    namespace Editor
    {
        namespace Create
        {
            namespace Scripts 
            {    
                public static class CreateGameData
                {
                    private const string NAME = "GameData";
                    private const string GAMEOBJECT_NAME = NAME;
                    private const string FILE_NAME = NAME;
                    private const string MENU_ITEM = "Kameosa/Create/Scripts/" + FILE_NAME;

                    [MenuItem(MENU_ITEM)]
                    public static void Main()
                    {
                        Common.CreateScriptFromTemplate(FILE_NAME, Constants.SCRIPTS_SERVICES_FOLDER_PATH);

                        EditorPrefs.SetBool(NAME, true);
                    }
                }
            }
        }
    }
}