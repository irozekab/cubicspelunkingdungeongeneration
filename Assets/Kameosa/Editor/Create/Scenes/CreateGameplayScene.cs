﻿using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

namespace Kameosa
{
    namespace Editor
    {
        namespace Create
        {
            namespace Scene
            {
                public static class CreateGameplayScene
                {
                    public const string NAME = "Gameplay";
                    private const string GAMEOBJECT_NAME = NAME;
                    private const string FILE_NAME = NAME + "Controller";
                    private const string MENU_ITEM = "Kameosa/Create/Scene/" + NAME;

                    [MenuItem(MENU_ITEM)]
                    public static void Main()
                    {
                        Common.CreateScene(NAME);

                        Common.CreateCanvas("Hud");
                        UnityEngine.GameObject scoreText = Common.CreateText("ScoreText", "000000");
                        scoreText.GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
                        Common.CreateText("Title", NAME);
                        Common.CreateButton("PauseButton");
                        UnityEngine.GameObject pausePanel = Common.CreatePanel("PausePanel");
                        UnityEngine.GameObject layoutContainer = Common.CreateLayoutContainer("LayoutContainer", pausePanel);
                        Common.CreateButton("ResumeButton", layoutContainer, null);
                        Common.CreateButton("QuitButton", layoutContainer, null);
                        Common.CreateEventSystem();

                        Common.CreateScriptFromTemplate(FILE_NAME, Constants.SCRIPTS_GAMEPLAY_FOLDER_PATH);

                        Scripts.CreateHudController.Main();

                        EditorPrefs.SetBool(NAME, true);
                    }

                    [UnityEditor.Callbacks.DidReloadScripts]
                    private static void OnDidReloadScripts()
                    {
                        if (EditorPrefs.HasKey(NAME))
                        {
                            EditorPrefs.DeleteKey(NAME);

                            if (Preferences.isPrintLog)
                            {
                                Debug.Log("Create" + NAME + ": OnDidReloadScripts()");
                            }

                            UnityEngine.GameObject gameObject = Common.CreateGameObject(NAME);
                            Common.AttachScriptToGameObject(Common.GetScriptPath(FILE_NAME, Constants.SCRIPTS_GAMEPLAY_FOLDER_PATH), gameObject);

                            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene(), Common.GetScenePath(NAME));
                        }
                    }
                }
            }
        }
    }
}