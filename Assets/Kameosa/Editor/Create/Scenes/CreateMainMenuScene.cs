﻿using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Kameosa
{
    namespace Editor
    {
        namespace Create
        {
            namespace Scene
            {
                public static class CreateMainMenuScene
                {
                    public const string NAME = "MainMenu";
                    private const string GAMEOBJECT_NAME = NAME;
                    private const string FILE_NAME = NAME + "Controller";
                    private const string MENU_ITEM = "Kameosa/Create/Scene/" + NAME;

                    [MenuItem(MENU_ITEM)]
                    public static void Main()
                    {
                        Common.CreateScene(NAME);

                        Common.CreateCanvas();
                        Common.CreatePanel("Background");
                        Common.CreateText("Title", "Main Menu");
                        UnityEngine.GameObject layoutContainer = Common.CreateLayoutContainer();
                        Common.CreateButton("PlayButton", layoutContainer);
                        Common.CreateButton("LeaderboardButton", layoutContainer);
                        Common.CreateEventSystem();

                        Common.CreateScriptFromTemplate(FILE_NAME, Constants.SCRIPTS_SCENES_FOLDER_PATH);

                        Services.CreateSceneTransitionService.Main();
                        Services.CreateGameManager.Main();

                        EditorPrefs.SetBool(NAME, true);
                    }

                    [UnityEditor.Callbacks.DidReloadScripts]
                    private static void OnDidReloadScripts()
                    {
                        if (EditorPrefs.HasKey(NAME))
                        {
                            EditorPrefs.DeleteKey(NAME);

                            Kameosa.Services.LogService.Info("Create" + NAME + ": OnDidReloadScripts()");

                            UnityEngine.GameObject gameObject = Common.CreateGameObject(GAMEOBJECT_NAME);
                            Common.AttachScriptToGameObject(Common.GetSceneControllerScriptPath(FILE_NAME), gameObject);

                            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene(), Common.GetScenePath(NAME));
                        }
                    }
                }
            }
        }
    }
}