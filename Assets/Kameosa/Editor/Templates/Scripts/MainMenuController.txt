﻿using System.Collections;
using System.Collections.Generic;
using Kameosa.Scene;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuController : BaseSceneController
{
    private void Start()
    {
    }

    private void Update()
    {
    }

    public void Quit()
    {
        Application.Quit();
    }
}