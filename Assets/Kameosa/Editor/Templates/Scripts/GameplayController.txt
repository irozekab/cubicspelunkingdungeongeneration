﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Kameosa.Gameplay2D;
using Kameosa.GameObject.Common.Controller;

public class GameplayController : Kameosa.GameObject.Common.Controller.BaseGameplayController 
{
    //[SerializeField]
    //private PlayerController playerController;

    //private int score = 0;

    #region Properties

    //public int Score
    //{
    //    get
    //    {
    //        return this.score;
    //    }
    //}

    #endregion

    private void Awake()
    {
        //if (this.playerController == null)
        //{
        //    this.playerController = GameObject.Find("Player").GetComponent<PlayerController>();
        //}
    }

    private void OnEnable()
    {
        //this.playerController.OnDie += OnPlayerDie;
    }

    private void Start()
    {
    }

    private void Update()
    {
    }

    private void OnDisable()
    {
        //this.playerController.OnDie -= OnPlayerDie;
    }

    protected override void GameAwake()
    {
		base.GameAwake();
    }
    
    public override void GameStart()
    {
		base.GameStart();

        Time.timeScale = 1f;
    }

    public override void GamePause()
    {
		base.GamePause();

        Time.timeScale = 0f;
    }

    public override void GameResume()
    {
		base.GameResume();

        Time.timeScale = 1f;
    } 

    protected override void GameWon()
    {
		base.GameWon();
    } 

    protected override void GameLose()
    {
		base.GameLose();
    } 

    protected override void GameEnd()
    {
		base.GameEnd();
 
        Time.timeScale = 0f;
    } 

    public override void GameRestart()
    {
		base.GameRestart();

        SceneTransitionController.Instance.ReloadScene();
        StartCoroutine(UnfreezeTimeScale());
    }

    public override void GameQuit()
    {
		base.GameQuit();

        SceneTransitionController.Instance.LoadScene("MainMenu");
        StartCoroutine(UnfreezeTimeScale());
    }

    //private void OnPlayerDie()
    //{
    //    GameLose();
    //}

    //private void OnGameWon()
    //{
    //    GameWon();
    //}
}