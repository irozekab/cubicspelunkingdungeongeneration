﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kameosa.Constants;
using Kameosa.Common;

public class OnTriggerDestroyer : Kameosa.Gameplay2D.BaseOnTriggerDestroyer
{
    protected override void Initialize()
    {
        IsDeactivateOnTrigger = true;
        //TagsToDestroy.Add("");
    }
}