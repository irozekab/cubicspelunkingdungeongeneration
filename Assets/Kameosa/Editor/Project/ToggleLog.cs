﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

namespace Kameosa
{
    namespace Editor
    {
        namespace Project
        {
            public class ToggleLog
            {
                [MenuItem("Kameosa/Project/Toggle Log", false, -1)]
                private static void Main()
                {
                    if (Kameosa.Services.LogService.IsLogEnabled)
                    {
                        Editor.Common.InformationDialog("Log Off", "Log has been turn off.");
                        Kameosa.Services.LogService.IsLogEnabled = false;
                    }
                    else
                    {
                        Editor.Common.InformationDialog("Log On", "Log has been turn on.");
                        Kameosa.Services.LogService.IsLogEnabled = true;
                    }
                }
            }
        }
    }
}
