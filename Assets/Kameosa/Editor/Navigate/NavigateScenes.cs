﻿using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;

namespace Kameosa
{
    namespace Editor
    {
        namespace Navigate
        {
            public class NavigateScenes
            {
                [MenuItem("Kameosa/Navigate/Main Menu %1", true)]
                public static bool ValidateNavigateToMainMenu()
                {
                    return System.IO.File.Exists(Path.Combine(Editor.Constants.SCENES_FOLDER_PATH, Create.Scene.CreateMainMenuScene.NAME + ".unity"));
                }

                [MenuItem("Kameosa/Navigate/Main Menu %1")]
                public static void NavigateToMainMenu()
                {
                    EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
                    EditorSceneManager.OpenScene(Path.Combine(Editor.Constants.SCENES_FOLDER_PATH, Create.Scene.CreateMainMenuScene.NAME + ".unity"));
                }

                [MenuItem("Kameosa/Navigate/Gameplay %2", true)]
                public static bool ValidateNavigateToGameplay()
                {
                    return System.IO.File.Exists(Path.Combine(Editor.Constants.SCENES_FOLDER_PATH, Create.Scene.CreateGameplayScene.NAME + ".unity"));
                }

                [MenuItem("Kameosa/Navigate/Gameplay %2")]
                public static void NavigateToGameplay()
                {
                    EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
                    EditorSceneManager.OpenScene(Path.Combine(Editor.Constants.SCENES_FOLDER_PATH, Create.Scene.CreateGameplayScene.NAME + ".unity"));
                }
            }
        }
    }
}
