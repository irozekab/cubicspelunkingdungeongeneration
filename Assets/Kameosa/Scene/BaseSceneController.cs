﻿using Kameosa.GameObject.Common.Controller;
using Kameosa.Services;
using UnityEngine;

namespace Kameosa
{
    namespace Scene
    {
        public abstract class BaseSceneController : MonoBehaviour
        {
            //protected virtual void Start()
            //{
            //}

            //protected virtual void Update()
            //{
            //}

            public virtual void LoadScene(string scene)
            {
                SceneTransitionController.Instance.LoadScene(scene);
            }
        }
    }
}