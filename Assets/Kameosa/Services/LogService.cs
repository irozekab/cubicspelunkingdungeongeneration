﻿using UnityEngine;

namespace Kameosa
{
    namespace Services
    {
        public static class LogService
        {
            private static ILogger logger = Debug.unityLogger;

            public static bool IsLogEnabled
            {
                get
                {
                    return LogService.logger.logEnabled;
                }

                set
                {
                    LogService.logger.logEnabled = value;
                }
            }

            public static void Info(string message, Object context = null)
            {
                Log(LogType.Log, message, context);
            }

            public static void Warning(string message, Object context = null)
            {
                Log(LogType.Warning, message, context);
            }

            public static void Error(string message, Object context = null)
            {
                Log(LogType.Error, message, context);
            }

            public static void Exception(string message, Object context = null)
            {
                Log(LogType.Exception, message, context);
            }

            public static void Assert(string message, Object context = null)
            {
                Log(LogType.Assert, message, context);
            }

            private static void Log(LogType logType, string message, Object context)
            {
                if (context == null)
                {
                    LogService.logger.Log(logType, "Kameosa", message);
                }
                else
                {
                    LogService.logger.Log(logType, "Kameosa", message, context);
                }
            }
        }
    }
}
