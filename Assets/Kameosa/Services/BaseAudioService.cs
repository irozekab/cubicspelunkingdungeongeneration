﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Kameosa
{
    namespace Services
    {
        public abstract class BaseAudioService : MonoBehaviour
        {
            protected AudioSource audioSource;
            private float volume;

            public float Volume
            {
                get
                {
                    return this.volume;
                }

                set
                {
                    this.volume = value;
                    this.audioSource.volume = value;
                    OnVolumeChanged();
                }
            }

            #region Singleton Pattern

            private static BaseAudioService instance;

            public static BaseAudioService Instance
            {
                get
                {
                    return BaseAudioService.instance;
                }
            }

            protected virtual void Awake()
            {
                if (BaseAudioService.instance != null)
                {
                    Destroy(this.gameObject);
                }
                else
                {
                    BaseAudioService.instance = this;
                    DontDestroyOnLoad(this.gameObject);
                }

                if (this.audioSource == null)
                {
                    this.audioSource = gameObject.GetComponent<AudioSource>();
                }
            }

            #endregion

            protected virtual void Start()
            {
                Initialize();
            }

            protected abstract void Initialize();

            protected virtual void PlayOnce()
            {
#if UNITY_EDITOR
                Kameosa.Services.LogService.Info("BaseAudioService.PlayOnce()");
#endif

                if (!this.audioSource.isPlaying)
                {
                    this.audioSource.Play();
                }
            }

            protected virtual void Stop()
            {
#if UNITY_EDITOR
                Kameosa.Services.LogService.Info("BaseAudioService.Stop()");
#endif

                if (this.audioSource.isPlaying)
                {
                    this.audioSource.Stop();
                }
            }

            protected virtual void Mute()
            {
#if UNITY_EDITOR
                Kameosa.Services.LogService.Info("BaseAudioService.Mute()");
#endif


                this.audioSource.mute = true;
            }

            protected virtual void Unmute()
            {
#if UNITY_EDITOR
                Kameosa.Services.LogService.Info("BaseAudioService.Unmute()");
#endif

                this.audioSource.mute = false;
            }

            protected virtual void OnVolumeChanged()
            {
#if UNITY_EDITOR
                Kameosa.Services.LogService.Info("BaseAudioService.OnVolumeChanged()");
#endif
            }
        }
    }
}