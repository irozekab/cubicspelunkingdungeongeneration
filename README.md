# CubicSpelunkingDungeonGeneration

### What is this repository for?

BasicCubicPlatformer is a very basic implementation of Spelunking dungeon generation along with a 2D platformer movement controller.

This is the oldest.
Version 1.

### What is the current state?

The Kameosa library is v0.3, it hasn't been updated since 201808 when I shift my focus to old-Rachel. Will need to revist when I wish to move back to 2D platformer.

Still have the dungeon generation algorithm.

### How do I get set up?

1. Download Unity and run Unity.
2. Update project to latest Unity version if necessary.
3. Fix compilation errors which might show up in future Unity version.
4. Start coding.
